###	May not be Auto

extends Node

const scores_path: String = "user://scores.dat"

enum SortType { SCORE = 0, PLAYTIME = 1, DATE = 2, NAME = 3, KILLS = 4 }

var highscores: Array
var scores_file: File

func _init() -> void:
	scores_file = File.new()
	
	if scores_file.file_exists(scores_path):
		load_highscores()		


func add_highscore(player_name: String, score: int, play_time: String, kills: int) -> void:
	var dt = OS.get_datetime()
	### 'date' and 'time' split up for easy reading ###
	var date = str(dt["month"]) + "/" + str(dt["day"]) + "/" + str(dt["year"])
	var time = str(dt["hour"]).pad_zeros(2) + ":" + str(dt["minute"]).pad_zeros(2)
#	var id = player_name + "_" + str(score) + "_" + date + time
#	highscores.append({"id" : id, "name" : player_name, "score" : str(score), "date" : date + " " + time})
	highscores.append({"name": player_name, "score": str(score), "playtime": play_time, "kills": str(kills), "date": date + " " + time})
	
	save_highscores()
	
	
func clear_highscores(delete_file: bool = false) -> void:
	highscores.clear()
	
	save_highscores()
	
	if delete_file:
		delete_scores_file()
		
		
### Currently Unused ###
func delete_highscore(highscore: Dictionary) -> void:
	var idx = -1
	var cnt = 0
	
	for score in highscores:
		if score["date"] == highscore["date"]:
			idx = cnt
			break
		cnt += 1
	
	highscores.remove(idx)	
	save_highscores()


func delete_scores_file() -> void:
	var dir = Directory.new()
	dir.remove(scores_path)


func load_highscores() -> void:
	open_read()
	
	highscores = scores_file.get_var()
	scores_file.close()


func open_read() -> void:
	scores_file.open(scores_path, File.READ)


func open_write() -> void:
	scores_file.open(scores_path, File.WRITE)


func save_highscores() -> void:
	open_write()
	
	if scores_file.file_exists(scores_path):
		delete_scores_file()
	
	scores_file.store_var(highscores)
	scores_file.close()
	
	
func sort_highscores(sort_type: int) -> void:
	if sort_type == SortType.SCORE:
		highscores.sort_custom(self, "sort_by_score")


func sort_by_score(a: Dictionary, b: Dictionary):
	if int(b["score"]) < int(a["score"]):
		return true
	return false
