extends ColorRect

func _ready() -> void:
	MusicManager.start_playback()
	
func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept"):
		if MusicManager.playing:
			MusicManager.stop_playback()
		else:
			MusicManager.start_playback()
	elif event.is_action_pressed("pause"):
		MusicManager.toggle_pause()
	elif event.is_action_pressed("ui_left"):
		MusicManager.previous_track()
	elif event.is_action_pressed("ui_right"):
		MusicManager.next_track()
	elif event.is_action_pressed("ui_end"):
		MusicManager.play_next_random()
