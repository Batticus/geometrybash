extends CanvasLayer

var points: int
var kills: int
var start_time: int

func _ready() -> void:
	points = 0
	kills = 0
	start_time = OS.get_unix_time()
	add_points(points)
	$Kills.set_text("Kills: 0")
	$TimePlayed.set_text("00:00")
	$PlayTimer.start()


func add_points(points_value: int) -> void:
	points += points_value
	$Points.set_text("Points: " + str(points))
	
	
func get_points() -> int:
	return (points)


func get_kills() -> int:
	return (kills)


func get_time_text() -> String:
	return ($TimePlayed.text)


func add_kill() -> void:
	kills += 1
	$Kills.set_text("Kills: " + str(kills))
	
	
func add_time() -> void:
	var elapsed = OS.get_unix_time() - start_time
	var minutes = elapsed / 60
	var seconds = elapsed % 60
	var str_time = "%02d:%02d" % [minutes, seconds]
	$TimePlayed.set_text(str_time)


func _on_PlayTimer_timeout() -> void:
	add_time()
