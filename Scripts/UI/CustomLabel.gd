extends Label

signal custom_label_mouse_pressed

var mouse_is_over: bool = false
var enable_hover_color: bool = false setget set_enable_hover_color, get_enable_hover_color

func _input(event) -> void:
	if event.is_action_pressed("mouse_left"):
		if mouse_is_over:
			emit_signal("custom_label_mouse_pressed", self)


func _on_mouse_entered() -> void:
	mouse_is_over = true
	if enable_hover_color:
		self.modulate = Color("699ce8")


func _on_mouse_exited() -> void:
	mouse_is_over = false
	if enable_hover_color:
		self.modulate = Color(1, 1, 1, 1)
	
	
func set_enable_hover_color(enable: bool) -> void:
	enable_hover_color = enable
	
	
func get_enable_hover_color() -> bool:
	return enable_hover_color