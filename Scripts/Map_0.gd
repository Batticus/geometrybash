extends Node2D

### 'Level' idea:
###
### Every 'n' amount of kills/points, a new 'level' is
### reached. These are some 'level' ideas.
###
### Level 1: Basic setup, as it always has been.
### 	start spawn time: 3
### 	min spawn time: 0.5 (maybe not so low for level 1)
### 	max spawn count: 1
### 	enemy speed: default
###
### Level 2: (? WIP ?)
### 	start spawn time: 2
### 	min spawn time: 0.4
### 	max spawn count: 2 (or sometimes 2)
### 	enemy speed: +15 (or something)

### Will all shapes share 'spawn_time_min' and 'spawn_time_max'? ###

#const square_spawn_time_min = 0.5
#const triangle_spawn_time_min = 0.5
#const square_spawn_time_max = 3.0
#const triangle_spawn_time_max = 3.0
#const square_spawn_count_max = 5
#const triangle_spawn_count_max = 5

### - Minimum amount of time between spawns - ###
const spawn_time_min: float = 0.5
### - Maximum amount of time between spawns - ###
const spawn_time_max: float = 3.0
### - Maximum amount of simultaneous spawns - ###
### If used, this variable will be used
### throughout the play session. 'Level'
### 1, it will have the value of 1,
### Level 2, value of 2, etc.
const spawn_count_max: int = 5
const square_threshold: int = 25	#15?
const triangle_threshold: int = 75
const spawn_increase_factor: int = 20
### 'spawn_weight' will increase every time 'kill_count' reaches 'spawn_weight_increase_threshold'
const spawn_weight_increase_threshold: int = 50
const init_circle_chance: int = 49		# 0 - 49
const init_square_chance: int = 29		# 50 - 79
const init_triangle_chance: int = 19	# 80 - 99

export (bool) var is_static: bool = false
export (PackedScene) var circle_particles: PackedScene
export (PackedScene) var player_particles: PackedScene
export (PackedScene) var square_particles: PackedScene
export (PackedScene) var triangle_particles: PackedScene

onready var _player: Area2D = $PlayerShape
onready var _environment: WorldEnvironment = $WorldEnvironment
onready var _shape_settings_window: Control = $PauseLayer/Pause/SettingsWindow/ShapeSettingsWindow

var _autoplay: bool = false
var _beyond_scores: bool = false
### The level of increased shape spawning
var _spawn_level: int
### DEBUG ###
var _debug_enabled: bool = false
var _circles_spawned: int
var _squares_spawned: int
var _triangle_spawned: int
### DEBUG ###

#var square_spawn_time
#var triangle_spawn_time
#var level: int #?
var bullet_target: Vector2
var enemies: Array
var player_start: Vector2
var score: int
var screen_size: Vector2
var spawn_time: float
# Amount of enemies to spawn at each spawn (NYI)
var spawn_count: int
var kill_count: int
# Amount of enemies that have been spawned
var spawned_count: int

var music_playing: bool = false
var paused: bool = false

var bullet_ps: PackedScene = preload("res://Scenes/Shapes/Bullet.tscn")
var enemy_square_ps: PackedScene = preload("res://Scenes/Shapes/SquareShape.tscn")
var enemy_circle_ps: PackedScene = preload("res://Scenes/Shapes/CircleShape.tscn")
var enemy_triangle_ps: PackedScene = preload("res://Scenes/Shapes/TriangleShape.tscn")
var pause_scene_ps: PackedScene = preload("res://Scenes/Windows/Pause.tscn")
var geobash_res_path: String = "res://Assets/Music/GeoBash.ogg"
var destruction_res_path: String = "res://Assets/Music/Destruction.ogg"
var rng: RandomNumberGenerator

# Point values are a work in progress
enum POINTS { Circle = 1, Square = 5, Triangle = 10 }

#enum TRACKS { GeoBash = 1, Destruction = 2 }

func _ready() -> void:
	randomize()	
	screen_size = get_viewport_rect().size
	player_start = Vector2(screen_size.x / 2, screen_size.y / 2)
	rng = RandomNumberGenerator.new()
	Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
	_shape_settings_window.connect("shape_settings_changed", self, "_on_shape_settings_changed")
	_player.connect("player_hit", self, "_on_player_hit")
	_player.connect("player_ascended", self, "_on_player_ascended")
	_player.set_color(Settings.player_color)
	_player.set_light_texture_size()
#	get_viewport().connect("size_changed", self, "_on_size_changed")
	
#	if Settings.is_html5 && !is_static:
#		$HUD/Polygon2D.polygon.set(0, Vector2(1, 1))
#		$HUD/Polygon2D.polygon.set(1, Vector2(screen_size.x - 1, 1))
#		$HUD/Polygon2D.polygon.set(2, Vector2(screen_size.x - 1, screen_size.y - 1))
#		$HUD/Polygon2D.polygon.set(3, Vector2(1, screen_size.y - 1))
#		$HUD/Polygon2D.show()
	
	### DEBUG ###
	$HUD/Debug.hide()
	### DEBUG ###
		
	$PauseLayer/Pause.connect("game_paused", self, "_on_game_paused")
	initialize()
#	start_game()
	if !is_static:
		startup()
	else:
		start_game()
	

###
###	DEBUG STUFF SHOULD GO IN DEBUG SCENE!!!
###
func _input(event: InputEvent) -> void:
	
	if event.is_action_pressed("next_track"):
		if MusicManager.play_sequentially:
			MusicManager.play_next_sequential()
		else:
			MusicManager.play_next_random()
	
	if event.is_action_pressed("previous_track"):
		if MusicManager.play_sequentially:
			MusicManager.play_next_sequential()
		else:
			MusicManager.play_next_random()
	
	### DEBUG ###	
	if event.is_action_pressed("toggle_debug"):
		_debug_enabled = !_debug_enabled
		if _debug_enabled:
			$HUD/Debug.show()
		else:
			$HUD/Debug.hide()
			
	if event.is_action_pressed("debug_add_kills") && _debug_enabled:
		kill_count += 50
		$HUD.kills += 50
		
	if event.is_action_pressed("debug_remove_kills") && _debug_enabled:
		kill_count -= 50
		$HUD.kills += 50

	### DEBUG ###
	
	if event.is_action_pressed("html5_fullscreen_toggle") && Version.is_html5:
		Settings.fullscreen = !Settings.fullscreen
#		OS.window_fullscreen = !OS.window_fullscreen
#		Settings.fullscreen = OS.window_fullscreen
		Settings.adjust_display_settings()
			
		
func _process(delta: float) -> void:
	
	### DEBUG ###	
	if _debug_enabled:
		$HUD/Debug/Label.set_text("Enemies: " + str(enemies.size()))
		$HUD/Debug/Label5.set_text("Kill Count: " + str(kill_count))
		$HUD/Debug/Label12.set_text("Spawn Time: " + str(spawn_time))
	### DEBUG
	
	
func initialize() -> void:
	_environment.environment.glow_enabled = Settings.glow_enabled
	_environment.environment.glow_intensity = Settings.glow_intensity
	_environment.environment.glow_strength = Settings.glow_strength
	_environment.environment.glow_bloom = Settings.glow_bloom
	
	
#func initialize_world_environment() -> void:
	
	
#	_environment = Settings.get_world_environment()
#	add_child(_environment)
	
#	if is_static && Settings.glow_enabled_menus:
#		add_child(_environment)
#	elif Settings.glow_enabled:
#		add_child(_environment)
	#	_environment.environment.glow_intensity = Settings.glow_intensity
	#	_environment.environment.glow_strength = Settings.glow_strength
	#	_environment.environment.glow_bloom = Settings.glow_bloom
#	pass


func determine_spawn_v1() -> void:
	### No. 3 ###
	### Idea: Separate timers for each enemy type. If the respective
	### timer for an enemy elapses, then we can perform a 'spawn_chance'
	### to see if the enemy will spawn, if it fails, the timer starts
	### again. Rinse and repeat.
	
	var allow_square = false
	var allow_triangle = false
	var spawn_chance = rand_range(1, 100)

	if $HUD.get_points() >= 10:
		allow_square = true;

	if $HUD.get_points() >= 75:
		allow_triangle = true;

	if spawn_chance > 0 && spawn_chance < 51:
		spawn(enemy_circle_ps, Settings.circle_color)
	elif spawn_chance > 50 && spawn_chance < 81:
		if allow_square:
			spawn(enemy_square_ps, Settings.square_color)
		else:
			spawn(enemy_circle_ps, Settings.circle_color)
	elif spawn_chance > 80:
		if allow_triangle:
			spawn(enemy_triangle_ps, Settings.triangle_color)
		else:
			spawn(enemy_circle_ps, Settings.circle_color)
	
	### Idea:
	### Instead of lowering spawn time after every spawn,
	### it may be preferable if the spawn was lowered less
	### less frequently, but by larger amounts.
	
	### OG ###	
	if spawn_time > spawn_time_min:
		spawn_time -= 0.1


### New
func determine_spawn_v2() -> void:	
	### spawn_increase_factor = 10
	### spawn_weight = kill_count * 0.01 (/100)
	### add_spawn_chance = spawn_weight * spawn_increase_factor
	### circle_chance = 50
	### square_chance = 80
	### triangle_chance = 100 [Not necessary?]
	
	### When kill_count == 100, spawn_weight == 1
	### When kill_count == 200, spawn_weight == 2
	### square_chance += add_spawn_chance
	###	spawn_chance = rand_range(1, 100 + (add_spawn_chance * 2))
	
	### spawn_chance < circle_chance
	###		...Circle
	### spawn_chance > circle_chance && spawn_chance < square_chance
	###		...Square
	### spawn_chance > square_chance
	###		...Triangle
	
	### Standard Spawn: (0 - 100) [Slightly Inaccurate - 0 - 50 is 51%, if 0 ever occurs]
	###		Circle Chance = 50% (0 - 50)
	###		Square Chance = 30% (51 - 80)
	###		Triangle Chance = 20% (81 - 100)
	### Next: (0 - 100 + add_spawn_chance [10]) [kill_count == 100]
	###		Circle Chance = 50% (0 - 50)
	###		Square Chance = 40% (51 - 90)
	###		Triangle Chance = 30% (91 - 110)
	### Next?: (0 - 100 + add_spawn_chance [20]) [kill_count == 200]
	###		Circle Chance = 50% (0 - 50)
	###		Square Chance = 50% (51 - 100)
	###		Triangle Chance = 40% (91 - 120)
	
	#############################################
	# It may be better to lower 'circle_chance' #
	# while increasing both 'square_chance' and #
	# 'triangle_chance', instead of adding more #
	# numbers to the 'range'.				    #
	#############################################
	# Not so ideal as is. When kills get too    #
	# high, square and triangle outweigh circle #
	# by a lot, and mostly squares will spawn   #
	# due to it's high spawn rate, and	        #
	# triangle and circles lower spawn rate     #
	#############################################
	# As is, _spawn_level will keep growing      #
	# the player dies. Maybe cap it at 5? This	#
	# will require making at least some of the  #
	# variable scene-accessible, and not local  #
	# to 'determine_spawn_v2()'					#
	#############################################
	
	rng.randomize()
	var allow_square: bool
	var allow_triangle: bool
	var spawn_chance: int
	### How much chance is added to 'spawn_chance' and 'square/triangle_chance'
	var add_spawn_chance: int
	var circle_chance: int
	var square_chance: int
	
	allow_square = $HUD.get_points() >= square_threshold
	allow_triangle = $HUD.get_points() >= triangle_threshold
	
	if _spawn_level < 4:
		_spawn_level = int(floor(kill_count / spawn_weight_increase_threshold)) #1.99 must still equal 1
	
	add_spawn_chance = _spawn_level * spawn_increase_factor
	circle_chance = 49
	square_chance = 79 + (add_spawn_chance * 0.5)
	
	spawn_chance = rng.randi_range(0, 99 + add_spawn_chance)
	
	### DEBUG ###	
	if _debug_enabled:
		$HUD/Debug/Label2.set_text("Spawn Chance: " + str(spawn_chance))
		$HUD/Debug/Label3.set_text("Spawn Level: " + str(_spawn_level))
		$HUD/Debug/Label4.set_text("Max Spawn Chance: " + str(99 + add_spawn_chance))
		$HUD/Debug/Label9.set_text("Circle Chance: " + str(circle_chance))
		$HUD/Debug/Label10.set_text("Square Chance: " + str(square_chance - circle_chance))
		$HUD/Debug/Label11.set_text("Triangle Chance: " + str((99 + add_spawn_chance) - square_chance))		
	### DEBUG ###
		
	if spawn_chance < circle_chance:
		spawn(enemy_circle_ps, Settings.circle_color)
		
		### DEBUG ###
		_circles_spawned += 1
		$HUD/Debug/Label6.set_text("Circles: " + str(_circles_spawned))
		### DEBUG ###
	elif spawn_chance > circle_chance && spawn_chance < square_chance:
		if allow_square:
			spawn(enemy_square_ps, Settings.square_color)
			
			### DEBUG ###
			_squares_spawned += 1
			$HUD/Debug/Label7.set_text("Squares: " + str(_squares_spawned))
			### DEBUG ###
		else:
			spawn(enemy_circle_ps, Settings.circle_color)
			
			### DEBUG ###
			_circles_spawned += 1
			$HUD/Debug/Label6.set_text("Circles: " + str(_circles_spawned))
			### DEBUG ###
	elif spawn_chance > square_chance:
		if allow_triangle:
			spawn(enemy_triangle_ps, Settings.triangle_color)
			
			### DEBUG ###
			_triangle_spawned += 1
			$HUD/Debug/Label8.set_text("Triangles: " + str(_triangle_spawned))
			### DEBUG ###
		else:
			spawn(enemy_circle_ps, Settings.circle_color)
			
			### DEBUG ###
			_circles_spawned += 1
			$HUD/Debug/Label6.set_text("Circles: " + str(_circles_spawned))
			### DEBUG ###
			
	if spawn_time > spawn_time_min:
		spawn_time -= 0.1


func determine_spawn_v3() -> void:
	rng.randomize()
	var allow_square: bool
	var allow_triangle: bool
	var spawn_chance: int
	var spawn_max: int
	var circle_chance: int = init_circle_chance
	var square_chance: int = init_square_chance
	var triangle_chance: int = init_triangle_chance	
	
	allow_square = $HUD.get_points() >= square_threshold
	allow_triangle = $HUD.get_points() >= triangle_threshold
			
	if _spawn_level < 4:
		_spawn_level = int(floor(kill_count / spawn_weight_increase_threshold)) #1.99 must still equal 1

	# _spawn_level > 2 for circle_chance 35 at _spawn_level 4
	# _spawn_level == 3 for circle_chance 30 at _spawn_level 4
	if _spawn_level > 2:
		circle_chance -= 5 * (_spawn_level - 1)
	else:
		circle_chance -= 5 * _spawn_level
		
	if _spawn_level > 0 && _spawn_level < 4:
		triangle_chance += 5 * (_spawn_level - 1)
	else:
		triangle_chance += 5 * _spawn_level
			
	square_chance += 5 * _spawn_level	
	spawn_max = circle_chance + square_chance + triangle_chance
	
	#cst => non-zero based
	# sl = 0	sl = 1	sl = 2	sl = 3	sl = 4
	# c = 50	c = 45	c = 40	c = 40	c = 35 / 30
	# s = 30	s = 35	s = 40	s = 45	s = 50
	# t = 20	t = 20	t = 25	t = 30	t = 40
	# 100		100		105		115		125
	
	spawn_chance = rng.randi_range(0, spawn_max)
	
	### DEBUG ###	
	if _debug_enabled:
		$HUD/Debug/Label2.set_text("Spawn Chance: " + str(spawn_chance))
		$HUD/Debug/Label3.set_text("Spawn Level: " + str(_spawn_level))
		$HUD/Debug/Label4.set_text("Max Spawn Chance: " + str(spawn_max))
		$HUD/Debug/Label9.set_text("Circle Chance: " + str(circle_chance))
		$HUD/Debug/Label10.set_text("Square Chance: " + str(square_chance))
		$HUD/Debug/Label11.set_text("Triangle Chance: " + str(triangle_chance))		
	### DEBUG ###
		
	if spawn_chance <= circle_chance:
		spawn(enemy_circle_ps, Settings.circle_color)
		
		### DEBUG ###
		_circles_spawned += 1
		$HUD/Debug/Label6.set_text("Circles: " + str(_circles_spawned))
		### DEBUG ###
	elif spawn_chance > circle_chance && spawn_chance <= circle_chance + square_chance:
		if allow_square:
			spawn(enemy_square_ps, Settings.square_color)
			
			### DEBUG ###
			_squares_spawned += 1
			$HUD/Debug/Label7.set_text("Squares: " + str(_squares_spawned))
			### DEBUG ###
		else:
			spawn(enemy_circle_ps, Settings.circle_color)
			
			### DEBUG ###
			_circles_spawned += 1
			$HUD/Debug/Label6.set_text("Circles: " + str(_circles_spawned))
			### DEBUG ###
	elif spawn_chance > circle_chance + square_chance:
		if allow_triangle:
			spawn(enemy_triangle_ps, Settings.triangle_color)
			
			### DEBUG ###
			_triangle_spawned += 1
			$HUD/Debug/Label8.set_text("Triangles: " + str(_triangle_spawned))
			### DEBUG ###
		else:
			spawn(enemy_circle_ps, Settings.circle_color)
			
			### DEBUG ###
			_circles_spawned += 1
			$HUD/Debug/Label6.set_text("Circles: " + str(_circles_spawned))
			### DEBUG ###	
			
	spawned_count += 1
			
	#
	#	NEW SPAWN TIMES ?
	#
			
#	if spawn_time > spawn_time_min:
#		spawn_time -= 0.05

	if spawn_time > spawn_time_min && spawned_count % 2 == 1:
		spawn_time -= 0.1
		
	# I'd rather not use all this 'if' statements. At the moment, I'm lazy.
	# I'll look into it
	# EXPERIMENTAL!
	if _spawn_level == 4:
		if spawn_time > 0.45 && kill_count > 200:
			spawn_time -= 0.05
		
		if spawn_time > 0.4 && kill_count > 250:
			spawn_time -= 0.05
			
		if spawn_time > 0.35 && kill_count > 300:
			spawn_time -= 0.05
			
		if spawn_time > 0.3 && kill_count > 350:
			spawn_time -= 0.05
		
#	match _spawn_level:
#		0:
#			if spawn_time > 2.5:
#				spawn_time -= 0.1
#		1:
#			if spawn_time > 2.0:
#				spawn_time -= 0.5
#		2:
#			if spawn_time > 1.5:
#				spawn_time -= 0.5
#		3:
#			if spawn_time > 1.0:
#				spawn_time -= 0.5
#		4:
#			if spawn_time > 0.5:
#				spawn_time -= 0.5


func start_game() -> void:
	score = 0
	if !_autoplay:
		spawn_time = 3.0
		MusicManager.start_playback(false)
	spawn_count = 1
	$SpawnTimer.wait_time = spawn_time
	_player.position = player_start
	$SpawnTimer.start()
	Settings.toggle_mute_audio(false)
	Settings.toggle_mute_effects(false)
	
	
func startup() -> void:
	var no_color = Color(0, 0, 0, 0)
	var tween_time = 2.0
	
	_player.set_frozen(true)
	
	$Tween.interpolate_property($PlayerShape/Sprite, "self_modulate", no_color, Settings.player_color, tween_time, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.interpolate_property($PlayerShape/Sprite/Light, "self_modulate", no_color, Settings.player_edge_color, tween_time, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.interpolate_property($PlayerShape/Pointer, "self_modulate", no_color, Settings.player_color, tween_time, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.interpolate_property($PlayerShape/Pointer/Light, "self_modulate", no_color, Settings.player_edge_color, tween_time, Tween.TRANS_LINEAR, Tween.EASE_IN)
#	$Tween.interpolate_property($WorldEnvironment, "environment/glow_intensity", 0, Settings.glow_intensity, tween_time, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()


func show_new_highscore_window() -> void:
	var highscore_ps = load("res://Scenes/Windows/NewHighScoreWindow.tscn")
	var highscore = highscore_ps.instance()
	
#	get_node("/root").add_child(highscore)
	add_child(highscore)
	
#	$HUD/PlayTimer.stop()
#	$SpawnTimer.stop()
#	$Explosion.play()
	highscore.set_score($HUD.get_points())
	highscore.set_kills($HUD.get_kills())
	highscore.set_play_time($HUD.get_time_text())
#
#	if music_playing:
#		toggle_music(false)


func end_game() -> void:
#	toggle_world_environment(false)
	if _beyond_scores:
		show_game_over_menu()
	else:
		show_new_highscore_window()
		
		
func get_enemy_count() -> int:
	return(len(enemies))
#	return enemy_count
	
	
#func set_enemy_count(null) -> void:
#	var msg = "Cannot externally change enemy count"
#	print_debug(msg)
#	print(msg)
#	enemy_count = get_enemy_count()


func spawn(enemy_ps, color) -> void:
	# Instantiate enemy
	var enemy = enemy_ps.instance()
	enemy.set_color(color)
	
	if Settings.shape_edges_enabled:
		enemy.show_light_texture()
		enemy.set_light_texture(load(Settings.get_light_res_path(enemy)))
	else:
		enemy.hide_light_texture()
		
	#Set spawn point offset
	$SpawnPath/SpawnPoint.set_offset(randi())
	enemy.connect("enemy_destroyed", self, "_on_enemy_destroyed")
	add_child(enemy)
	# Set enemy position
	enemy.position = $SpawnPath/SpawnPoint.position
#	enemy_count += 1
	enemies.append(enemy)
	
	
func show_explosion(node: Node) -> void:
#	var explosion_ps: PackedScene
	var explosion: Node
	var is_enemy: bool
	
	if node.is_in_group("Circle"):
#		explosion_ps = load("res://Scenes/Particles/Explosion3/Explosion_Circle_3.tscn")
		explosion = circle_particles.instance()
		is_enemy = true
	elif node.is_in_group("Square"):
#		explosion_ps = load("res://Scenes/Particles/Explosion3/Explosion_Square_3.tscn")
		explosion = square_particles.instance()
		is_enemy = true
	elif node.is_in_group("Triangle"):
#		explosion_ps = load("res://Scenes/Particles/Explosion3/Explosion_Triangle_3.tscn")
		explosion = triangle_particles.instance()
		is_enemy = true
	elif node.is_in_group("Player"):
#		explosion_ps = load("res://Scenes/Particles/Explosion3/Explosion_Player_3.tscn")
		explosion = player_particles.instance()
		is_enemy = false
		
#	explosion = explosion_ps.instance()
	explosion.set_color(node.get_color())
	explosion.position = node.position

	### Particle scenes ending in "_2" are particles
	### that travel in a specific direction. If we choose
	### to use said particles, we need to determine the
	### direction in which the particles will emit.
	### To do this, we will need to know the direction in
	### which the bullet was travelling when it hit the enemy,
	### and set the particles to emit in the same direction.
	### This will prove to be a bit of a job, as we will need
	### to pass a variable storing the direction between many scenes
	if explosion.get_name().ends_with("_2") && is_enemy:
		explosion.determine_rotation(node)

	add_child(explosion)
	explosion.emit()
	
	
func show_game_over_menu() -> void:
	var game_over_ps = load("res://Scenes/Windows/GameOverScene.tscn")
	var game_over = game_over_ps.instance()
	get_node("/root").add_child(game_over)
	
	
#func toggle_world_environment(enable: bool) -> void:
#	pass
#	if enable:
#		initialize_world_environment()
#	else:
#		_environment.environment.glow_enabled = false
#	$WorldEnvironment.environment.glow_enabled = enable


#func toggle_music(play: bool) -> void:
#	if play:
#		$Music.play()
#	else:
#		$Music.stop()
#
#	music_playing = play


#func toggle_effects(play: bool) -> void:
#	if play:
#		Settings.effects_audio_bus
#	else:
#		$Explosion.volume_db = Settings.effects_volume


func _on_SpawnTimer_timeout() -> void:
	$SpawnTimer.stop()
	
#	determine_spawn_v1()
#	determine_spawn_v2()
	determine_spawn_v3()
		
	$SpawnTimer.wait_time = spawn_time
	$SpawnTimer.start()


func _on_PlayerShape_shoot_standard() -> void:
	if _autoplay || _player.get_frozen():
		return
	var bullet = bullet_ps.instance()
#	bullet.connect("bullet_freed", self, "_on_bullet_freed")
	bullet.modulate = Settings.bullet_color
	add_child(bullet)
	bullet.fire($PlayerShape/Pointer/BulletStart.global_position, get_global_mouse_position())
	$PlayerShape/ShootSound.play()
	
	
func _on_player_ascended() -> void:
	_beyond_scores = true
	
	
func _on_shape_settings_changed() -> void:
	_player.set_color(Settings.player_color)
	_player.set_light_texture_size()
	_player.do_color_correction()
	
	
#func _on_size_changed() -> void:
#	Settings.adjust_display_settings()


func _on_enemy_destroyed(enemy: Node) -> void:
	### This can be used if we want the music to start
	### when the first enemy is destroyed. Just a novel
	### idea, but it's a bit jarring.
	#	if !music_playing && has_node("PlayerShape"):
	#		toggle_music(true)
	
	$Explosion.play()
	show_explosion(enemy)
		
	$HUD.add_kill()
	kill_count += 1
	if "CircleShape" in enemy.get_name():
		$HUD.add_points(POINTS.Circle)
	if "SquareShape" in enemy.get_name():
		$HUD.add_points(POINTS.Square)
	if "TriangleShape" in enemy.get_name():
		$HUD.add_points(POINTS.Triangle)
		
	enemies.erase(enemy)


func _on_player_hit() -> void:
	_player.hide()
	show_explosion(_player)
	_player.queue_free()
	$PauseLayer.queue_free()
	end_game()
		
	$HUD/PlayTimer.stop()
	$SpawnTimer.stop()
	$Explosion.play()
	
	if MusicManager.playing:
		MusicManager.stop_playback()
#	if music_playing:
#		toggle_music(false)


func _on_game_paused() -> void:
	paused = !paused


func _on_Tween_tween_completed(object, key):
	_player.set_frozen(false)
	start_game()
