extends Node

signal ok_button_pressed
signal cancel_button_pressed

func _on_OKButton_pressed():
	emit_signal("ok_button_pressed")


func _on_CancelButton_pressed():
	emit_signal("cancel_button_pressed")
