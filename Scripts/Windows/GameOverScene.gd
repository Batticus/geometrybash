extends CanvasLayer

var keyboard_control: bool = false

func _ready() -> void:
	$GameOverScene/VBoxContainer.grab_focus()
	
	if Version.is_html5:
		$GameOverScene/VBoxContainer/ExitButton.hide()


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_up") or event.is_action_pressed("ui_down"):
		if !keyboard_control:
			keyboard_control = true
			$GameOverScene/VBoxContainer/RestartButton.grab_focus()


func _on_RestartButton_pressed() -> void:
	self.queue_free()
	get_tree().reload_current_scene()


func _on_ExitButton_pressed() -> void:
	get_tree().quit()


func _on_MainMenuButton_pressed() -> void:
	self.queue_free()
	get_tree().change_scene("res://Scenes/Windows/MainMenu.tscn")


func _on_RestartButton_mouse_entered() -> void:
	$GameOverScene/VBoxContainer/RestartButton.grab_focus()


func _on_MainMenuButton_mouse_entered() -> void:
	$GameOverScene/VBoxContainer/MainMenuButton.grab_focus()


func _on_ExitButton_mouse_entered() -> void:
	$GameOverScene/VBoxContainer/ExitButton.grab_focus()
