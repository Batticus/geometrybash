extends Node

var is_editable: bool = false setget set_editable, get_editable

func _ready() -> void:
#	$ConfirmationDialog.get_cancel().connect("pressed", self, "_popup_cancel")
#	$ConfirmationDialog.get_ok().connect("pressed", self, "_popup_ok")
	$ConfirmationPopup.connect("ok_button_pressed", self, "_on_popup_ok")
	$ConfirmationPopup.connect("cancel_button_pressed", self, "_on_popup_cancel")
	
	if !is_editable:
		$ClearButton.hide()
#	else:
#		$ClearButton.show()


func _on_CloseButton_pressed() -> void:
	self.queue_free()


func _on_ClearButton_pressed() -> void:
	if len($HighScorePanel.gamestats.highscores) > 0:
#		$ConfirmationDialog.popup_centered()
		$ConfirmationPopup.popup_centered()
		
		
func _on_popup_cancel() -> void:
	$ConfirmationPopup.hide()


func _on_popup_ok() -> void:
	$ConfirmationPopup.hide()
	$HighScorePanel.clear_scores(true)

	
func get_editable() -> bool:
	return is_editable


func set_editable(editable: bool) -> void:
	is_editable = editable
	$HighScorePanel.set_editable(is_editable)
