extends Control


func display_window() -> void:
	self.show()
#	$PanelContainer.show()
#	$MenuLayer.layer = 2
	
	
func hide_window() -> void:
	self.hide()
#	$PanelContainer.hide()
#	$MenuLayer.layer = 1


func _on_BfxrLink_pressed():
	OS.shell_open(Settings.bfx_url)
	
	
func _on_BoscaLink_pressed():
	OS.shell_open(Settings.bosca_ceoil_url)


func _on_GodotLink_pressed():
	OS.shell_open(Settings.godot_engine_url)


func _on_RedditThreadLink_pressed():
	OS.shell_open(Settings.reddit_thread_url)


func _on_CloseButton_pressed():
	self.hide_window()


func _on_FontPageLink_pressed():
	OS.shell_open(Settings.dafont_font_page)


func _on_FontCreatorLink_pressed():
	OS.shell_open(Settings.dafont_creator_profile)


func _on_AudacityLink_pressed():
	OS.shell_open(Settings.audacity_url)
