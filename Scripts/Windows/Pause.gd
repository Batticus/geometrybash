extends ColorRect

signal game_paused

onready var settings: Node = get_node("SettingsWindow")

var keyboard_control: bool = false

func _ready() -> void:
	settings.connect("close_button_pressed", self, "_on_close_button_pressed")
	
	if Version.is_html5:
		$PauseVBox/ExitButton.hide()

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("pause"):
		### To allow user to unpause using "pause" action,
		### remove the if statement. However, doing this
		### will also allow the user to "cheat." By pausing
		### the game, the user will be able to take aim while
		### paused, quickly unpause, and fire to always hit
		### an enemy.
		if !get_tree().paused:
			pause()
			
	if get_tree().paused:
		if event.is_action_pressed("ui_up") or event.is_action_pressed("ui_down"):
			if !keyboard_control:
				keyboard_control = true
				$PauseVBox/ResumeButton.grab_focus()
				
#func _unhandled_input(event):
#	if event.is_action_pressed("ui_up") or event.is_action_pressed("ui_down"):
#			if !keyboard_control:
#				keyboard_control = true
#				$PauseVBox/ResumeButton.grab_focus()	


func pause() -> void:
	if !get_parent().get_parent().is_static:
		var paused = get_tree().paused
		get_tree().paused = !paused
		visible = !paused
		emit_signal("game_paused")


func _on_close_button_pressed() -> void:
	settings.hide()


func _on_ResumeButton_pressed() -> void:
	pause()


func _on_MainMenuButton_pressed() -> void:
	get_tree().change_scene("res://Scenes/Windows/MainMenu.tscn")
	get_tree().paused = false


func _on_ExitButton_pressed() -> void:
	get_tree().quit()


func _on_SettingsButton_pressed() -> void:
	settings.show()
	keyboard_control = false
	### HACK ###
	### A little hack to ensure 'SettingsWindow' has focus
	### and to allow Enter key to apply change
	settings.grab_focus()
	
func _on_ResumeButton_mouse_entered() -> void:
	$PauseVBox/ResumeButton.grab_focus()


func _on_HighscoresButton_pressed() -> void:
	var viewer_ps = load("res://Scenes/Windows/HighScorePanelWButton.tscn")
	var viewer = viewer_ps.instance()
	viewer.get_node("HighScorePanelWButton").set_editable(false)
#	viewer.set_editable(false)
	add_child(viewer)


func _on_SettingsButton_mouse_entered() -> void:
	$PauseVBox/SettingsButton.grab_focus()


func _on_MainMenuButton_mouse_entered() -> void:
	$PauseVBox/MainMenuButton.grab_focus()


func _on_ExitButton_mouse_entered() -> void:
	$PauseVBox/ExitButton.grab_focus()

func _on_HighscoresButton_mouse_entered():
	$PauseVBox/HighscoresButton.grab_focus()
