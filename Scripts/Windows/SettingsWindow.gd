extends Control

### Signal will have to go into parent 'Control' node to work
### -OR-
### Attach this script to parent 'Control' node, and change onready var values
signal close_button_pressed
signal settings_applied
signal settings_window_hidden

onready var player: Node = $SettingsWindow/VBoxContainer/HBoxColors/VBoxPlayer/PlayerTexture
onready var bullet: Node = $SettingsWindow/VBoxContainer/HBoxColors/VBoxBullet/BulletTexture
onready var circle: Node = $SettingsWindow/VBoxContainer/HBoxColors/VBoxCircle/CircleTexture
onready var square: Node = $SettingsWindow/VBoxContainer/HBoxColors/VBoxSquare/SquareTexture
onready var triangle: Node = $SettingsWindow/VBoxContainer/HBoxColors/VBoxTriangle/TriangleTexture
onready var master_slider: Node = $SettingsWindow/VBoxContainer/VBoxAudio/GridAudio/MasterSlider
onready var music_slider: Node = $SettingsWindow/VBoxContainer/VBoxAudio/GridAudio/MusicSlider
onready var effects_slider: Node = $SettingsWindow/VBoxContainer/VBoxAudio/GridAudio/EffectsSlider
onready var fullscreen_checkbox: Node = $SettingsWindow/VBoxContainer/HBoxFullscreen/FullscreenCheckBox
onready var master_volume_label: Node = $SettingsWindow/VBoxContainer/VBoxAudio/GridAudio/MasterVolume
onready var music_volume_label: Node = $SettingsWindow/VBoxContainer/VBoxAudio/GridAudio/MusicVolume
onready var effects_volume_label: Node = $SettingsWindow/VBoxContainer/VBoxAudio/GridAudio/EffectsVolume
onready var menu_autoplay_checkbox: CheckBox = $SettingsWindow/VBoxContainer/HBoxMenuAutoplay/MenuAutoplayCheckBox
onready var glow_settings_button: Button = $SettingsWindow/VBoxContainer/VBoxVisuals/GlowSettingsButton
onready var glow_settings_window: Node = $GlowSettingsWindow
onready var shape_settings_button: Button = $SettingsWindow/VBoxContainer/VBoxVisuals/ShapeSettingsButton
onready var shape_settings_window: Node = $ShapeSettingsWindow
onready var master_bus_index: int = AudioServer.get_bus_index("Master")
onready var music_bus_index: int = AudioServer.get_bus_index("Music")
onready var effects_bus_index: int = AudioServer.get_bus_index("Effects")
onready var shoot_sound: AudioStreamPlayer = $ShootSound

var mouse_over_object: Node
var current_object: Node

func _ready() -> void:
	mouse_over_object = null
	current_object = null
	
	glow_settings_window.connect("glow_settings_hidden", self, "_on_glow_settings_hidden")
	
	### An AudioStreamPlayer2D was used for parity with the actual sound during
	### gameplay. This just sets the location of the sound to the centre of the
	### screen so that is sounds central.
	$ShootSound.position = Vector2(get_viewport_rect().size.x / 2, get_viewport().size.y / 2)
	
	if Version.is_html5:
		$SettingsWindow/VBoxContainer/HBoxFullscreen.hide()
		
	### load_settings must go in visibilty changed
#	load_settings()


func apply_settings_changes() -> void:
	Settings.master_volume = master_slider.value / 100
	Settings.music_volume = music_slider.value / 100
	Settings.effects_volume = effects_slider.value / 100
	Settings.fullscreen = fullscreen_checkbox.pressed
	Settings.menu_autoplay = menu_autoplay_checkbox.pressed
#	Settings.glow_enabled = enable_glow_checkbox.pressed
#	Settings.glow_intensity = 
#	Settings.glow_strength = 
#	Settings.glow_bloom = 

	if Version.is_html5:
		Settings.fullscreen = false

	Settings.adjust_global_settings()


func hide_window() -> void:
	emit_signal("close_button_pressed")


func hide_color_picker() -> void:
	current_object = null
	
	
func hide_parent_menu() -> void:
	if get_parent().get_name() == "Pause":
		get_parent().hide()
	elif get_parent().get_name() == "MenuLayer":
		get_parent().get_node("VBoxContainer").hide()
		get_parent().get_node("Title").hide()
	
	
func load_settings() -> void:
	player.modulate = Settings.player_color
	bullet.modulate = Settings.bullet_color
	circle.modulate = Settings.circle_color
	square.modulate = Settings.square_color
	triangle.modulate = Settings.triangle_color
	master_slider.value = Settings.master_volume * 100
	music_slider.value = Settings.music_volume * 100
	effects_slider.value = Settings.effects_volume * 100
	fullscreen_checkbox.pressed = Settings.fullscreen
	menu_autoplay_checkbox.pressed = Settings.menu_autoplay
#	enable_glow_checkbox.pressed = Settings.glow_enabled


func reset_color(object: Node) -> void:
	var def_color
	match object:
		player:
			def_color = Settings.player_color
		bullet:
			def_color = Settings.bullet_color
		circle:
			def_color = Settings.circle_color
		square:
			def_color = Settings.square_color
		triangle:
			def_color = Settings.triangle_color
	
	object.modulate = def_color
	
	
func show_parent_menu() -> void:
	if get_parent().get_name() == "Pause":
		get_parent().show()
	elif get_parent().get_name() == "MenuLayer":
		get_parent().get_node("VBoxContainer").show()
		get_parent().get_node("Title").show()


func _on_ApplyButton_pressed() -> void:
	apply_settings_changes()
	Settings.save_settings()
	hide_window()
	emit_signal("settings_applied")
#	self.release_focus()


func _on_CloseButton_pressed() -> void:
	hide_window()


func _on_MasterSlider_value_changed(value: int) -> void:
	AudioServer.set_bus_volume_db(master_bus_index, linear2db(value * 0.01))
	master_volume_label.text = str(value)


func _on_MusicSlider_value_changed(value: int) -> void:
	AudioServer.set_bus_volume_db(music_bus_index, linear2db(value * 0.01))
	music_volume_label.text = str(value)


func _on_EffectsSlider_value_changed(value: int) -> void:
	AudioServer.set_bus_volume_db(effects_bus_index, linear2db(value * 0.01))
	effects_volume_label.text = str(value)
	
	
func _on_glow_settings_hidden() -> void:
	self.show()
	show_parent_menu()


#func _on_SettingsWindow_gui_input(event: InputEvent) -> void:
#	if event.is_action_pressed("mouse_left"):
#		if mouse_over_object == null && color_picker_panel.visible:
#			hide_color_picker()
#		elif mouse_over_object != null && mouse_over_object != color_picker:
#			show_color_picker(mouse_over_object)
#	elif event.is_action_pressed("mouse_right"):
#		if color_picker_panel.visible:
#			hide_color_picker()
#		else:
#			if mouse_over_object != null && mouse_over_object != color_picker:
#				reset_color(mouse_over_object)


func _on_VBoxPlayer_mouse_entered() -> void:
	mouse_over_object = player


func _on_VBoxBullet_mouse_entered() -> void:
	mouse_over_object = bullet


func _on_VBoxCircle_mouse_entered() -> void:
	mouse_over_object = circle


func _on_VBoxSquare_mouse_entered() -> void:
	mouse_over_object = square


func _on_VBoxTriangle_mouse_entered() -> void:
	mouse_over_object = triangle


#func _on_ColorPicker_mouse_entered() -> void:
#	mouse_over_object = color_picker


func _on_mouse_exited() -> void:
	mouse_over_object = null


#func _on_ColorPicker_color_changed(color) -> void:
#	if current_object:
#		current_object.modulate = Color(color.to_html())


func _on_FullscreenCheckBox_pressed() -> void:
	
	### HACK ###
	### A little hack to ensure 'SettingsWindow' has focus
	### and to allow Enter key to apply changes
	grab_focus()


func _on_DefaultColorsButton_pressed() -> void:
	player.modulate = Color(Settings.def_player_color)
	bullet.modulate = Color(Settings.def_bullet_color)
	circle.modulate = Color(Settings.def_circle_color)
	square.modulate = Color(Settings.def_square_color)
	triangle.modulate = Color(Settings.def_triangle_color)
	
	### HACK ###
	### A little hack to ensure 'SettingsWindow' has focus
	### and to allow Enter key to apply change
	grab_focus()


func _on_GlowSettingsButton_pressed() -> void:
	glow_settings_window.display_window()
	self.hide()
	hide_parent_menu()


func _on_SettingsWindow_visibility_changed():
	if visible:
		load_settings()


func _on_ShapeSettingsButton_pressed():
	shape_settings_window.display_window()


func _on_EffectsSlider_gui_input(event):
	if event.is_action_released("mouse_left"):
		shoot_sound.play()
