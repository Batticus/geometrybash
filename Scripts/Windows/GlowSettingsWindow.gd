extends Control

signal glow_settings_hidden

#onready var circle_shape: Sprite = $MockView/Circle
#onready var player_shape: Sprite = $MockView/Player
#onready var player_pointer: Sprite = $MockView/Player/Pointer
#onready var bullet: Sprite = $MockView/Bullet
#onready var square_shape: Sprite = $MockView/Square
#onready var triangle_shape: Sprite = $MockView/Triangle
#onready var circle_edge: Sprite = $MockView/Circle/Edge
#onready var player_edge: Sprite = $MockView/Player/Edge
#onready var player_pointer_edge: Sprite = $MockView/Player/Pointer/Edge
#onready var square_edge: Sprite = $MockView/Square/Edge
#onready var triangle_edge: Sprite = $MockView/Triangle/Edge
onready var panel_container: PanelContainer = $PanelLayer/PanelContainer
onready var enable_glow_checkbox: CheckBox = $PanelLayer/PanelContainer/VBoxContainer/HBoxContainer/VBoxChecks/EnableGlowCheckBox
#onready var enable_menu_glow_checkbox: CheckBox = $PanelLayer/PanelContainer/VBoxContainer/HBoxContainer/VBoxChecks/EnableMenuGlowCheckBox
onready var glow_intensity_value_label: Label = $PanelLayer/PanelContainer/VBoxContainer/HBoxContainer/GridGlowSettings/GlowIntensityValueLabel
onready var glow_strength_value_label: Label = $PanelLayer/PanelContainer/VBoxContainer/HBoxContainer/GridGlowSettings/GlowStrengthValueLabel
onready var glow_bloom_value_label: Label = $PanelLayer/PanelContainer/VBoxContainer/HBoxContainer/GridGlowSettings/GlowBloomValueLabel
onready var glow_intensity_slider: HSlider = $PanelLayer/PanelContainer/VBoxContainer/HBoxContainer/GridGlowSettings/GlowIntensitySlider
onready var glow_strength_slider: HSlider = $PanelLayer/PanelContainer/VBoxContainer/HBoxContainer/GridGlowSettings/GlowStrengthSlider
onready var glow_bloom_slider: HSlider = $PanelLayer/PanelContainer/VBoxContainer/HBoxContainer/GridGlowSettings/GlowBloomSlider
onready var layer: CanvasLayer = $PanelLayer
#onready var world: WorldEnvironment = $WorldEnvironment

#var enable_glow: bool
#var enable_menu_glow: bool
var environment: Environment


func _ready() -> void:
	### Changing the layer so that it does not block
	### other menus, even though it's hidden.
	layer.layer = 0
	
	
func display_window() -> void:
	layer.layer = 3
	self.show()
	panel_container.show()
	initialize()
	
	
func hide_window() -> void:
	self.hide()
	panel_container.hide()
	layer.layer = 0
	emit_signal("glow_settings_hidden")
	
	
func initialize() -> void:
	environment = load("res://Resources/Environments/GameEnvironment.tres")
	
	enable_glow_checkbox.pressed = Settings.glow_enabled	
	glow_intensity_slider.value = Settings.glow_intensity
	glow_strength_slider.value = Settings.glow_strength
	glow_bloom_slider.value = Settings.glow_bloom
	
	
func restore_glow_settings() -> void:
	environment.glow_enabled = Settings.glow_enabled
	environment.glow_intensity = Settings.glow_intensity
	environment.glow_strength = Settings.glow_strength
	environment.glow_bloom = Settings.glow_bloom
	
	
func save_glow_settings() -> void:
	Settings.glow_enabled = enable_glow_checkbox.pressed
#	Settings.glow_enabled_menus = enable_menu_glow
	Settings.glow_intensity = glow_intensity_slider.value
	Settings.glow_strength = glow_strength_slider.value
	Settings.glow_bloom = glow_bloom_slider.value
	Settings.save_settings()
	
	
#func toggle_edges(enabled: bool) -> void:
#	circle_edge.visible = enabled
#	player_edge.visible = enabled
#	player_pointer_edge.visible = enabled
#	square_edge.visible = enabled
#	triangle_edge.visible = enabled
	
	
#func toggle_glow(enabled: bool) -> void:
#	if visible:
#		environment.glow_enabled = enabled
#		toggle_sliders_editable(enabled)


func toggle_sliders_editable(editable: bool) -> void:
	glow_intensity_slider.editable = editable
	glow_strength_slider.editable = editable
	glow_bloom_slider.editable = editable
	

func _on_GlowIntensitySlider_value_changed(value: float) -> void:
#	if visible:
	# Change label value
	glow_intensity_value_label.set_text(str(value))
	# Change $WorldEnvironment.environment.glow_intesity
	environment.glow_intensity = value


func _on_GlowStrengthSlider_value_changed(value: float) -> void:
#	if visible:
	glow_strength_value_label.set_text(str(value))
	environment.glow_strength = value


func _on_GlowBloomSlider_value_changed(value: float) -> void:
#	if visible:
	glow_bloom_value_label.set_text(str(value))
	environment.glow_bloom = value


func _on_GlowSettingsWindow_visibility_changed() -> void:
	pass
#	if visible:
#		initialize()
#		load_settings()
#	if visible:
#		add_child(Settings.get_world_environment())
#		world = get_node("WorldEnvironment")
#		toggle_glow(enable_glow)
#	else:
#		world.queue_free()
#	elif !Settings.glow_enabled_menus:
#		world.queue_free()


func _on_EnableGlowCheckBox_pressed() -> void:
#	enable_glow = enable_glow_checkbox.pressed
#	toggle_glow(enable_glow)
	environment.glow_enabled = enable_glow_checkbox.pressed
	toggle_sliders_editable(enable_glow_checkbox.pressed)


func _on_ApplyButton_pressed() -> void:
	save_glow_settings()
	hide_window()


func _on_CancelButton_pressed() -> void:
	restore_glow_settings()
	hide_window()


func _on_ResetValuesButton_pressed():
	glow_intensity_slider.value = Settings.def_glow_intensity
	glow_strength_slider.value = Settings.def_glow_strength
	glow_bloom_slider.value = Settings.def_glow_bloom
