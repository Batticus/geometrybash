extends Node

onready var name_text_edit = $NewHighscoreWindow/VBoxContainer/VBoxTextButtons/HBoxNameText/NameTextEdit
onready var ok_button = $NewHighscoreWindow/VBoxContainer/VBoxTextButtons/HBoxContainer/OKButton
onready var score_value = $NewHighscoreWindow/VBoxContainer/HBoxScore/ScoreValueLabel
onready var kills_value = $NewHighscoreWindow/VBoxContainer/HBoxKills/KillsValueLabel
onready var play_time_value = $NewHighscoreWindow/VBoxContainer/HBoxTime/TimeValueLabel

var score
var kills
var play_time

func _ready() -> void:
#	$VBoxContainer/VBoxTextButtons/HBoxNameText/NameTextEdit.grab_focus()
	pass


func set_score(par_score: int) -> void:
	if par_score == null:
		par_score = 0
		
	score = par_score
	score_value.text = str(score)


func set_kills(par_kills: int) -> void:
	if par_kills == null:
		par_kills = 0
		
	kills = par_kills
	kills_value.text = str(kills)


func set_play_time(par_play_time: String) -> void:
	if par_play_time == null:
		par_play_time = "00:00"
		
	play_time = par_play_time
	play_time_value.text = play_time


func show_game_over() -> void:
	var game_over_scene_ps = load("res://Scenes/Windows/GameOverScene.tscn")
	var game_over = game_over_scene_ps.instance()
	get_parent().add_child(game_over)
	get_parent().remove_child(self)
#	get_node("/root").add_child(game_over)
#	get_node("/root").remove_child(self)


func _on_NameTextEdit_text_changed(new_text: String) -> void:
	if !name_text_edit.text.empty():
		ok_button.disabled = false
	else:
		ok_button.disabled = true


func _on_OKButton_pressed() -> void:
	var gs = load("res://Scripts/GameStats.gd")
	var gamestats = gs.new()
	gamestats.add_highscore(name_text_edit.text, score, play_time, kills)
	show_game_over()


func _on_CancelButton_pressed() -> void:
	show_game_over()
