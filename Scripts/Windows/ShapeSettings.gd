extends Control

signal shape_settings_changed

onready var shape_settings_panel: PanelContainer = $MenuLayer/ShapeSettings
onready var edges_enabled_checkbox: CheckBox = $MenuLayer/ShapeSettings/VBoxContainer/HBoxEdgesEnabled/EdgesEnabledCheckBox
onready var slider_grid: GridContainer = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeSize/GridContainer
onready var override_edge_checkbox: CheckBox = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeColor/HBoxOverrideEdge/OverrideEdgeCheckBox
onready var button_grid: GridContainer = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeColor/GridContainer
onready var player_texture: TextureRect = $MenuLayer/ShapeSettings/VBoxContainer/HBoxColors/VBoxPlayer/PlayerTexture
onready var bullet_texture: TextureRect = $MenuLayer/ShapeSettings/VBoxContainer/HBoxColors/VBoxBullet/BulletTexture
onready var circle_texture: TextureRect = $MenuLayer/ShapeSettings/VBoxContainer/HBoxColors/VBoxCircle/CircleTexture
onready var square_texture: TextureRect = $MenuLayer/ShapeSettings/VBoxContainer/HBoxColors/VBoxSquare/SquareTexture
onready var triangle_texture: TextureRect = $MenuLayer/ShapeSettings/VBoxContainer/HBoxColors/VBoxTriangle/TriangleTexture
onready var player_edge: TextureRect = $MenuLayer/ShapeSettings/VBoxContainer/HBoxColors/VBoxPlayer/PlayerTexture/PlayerEdge
#onready var bullet_edge: TextureRect = $ShapeSettings/VBoxContainer/HBoxColors/VBoxBullet/BulletTexture/BulletEdge
onready var circle_edge: TextureRect = $MenuLayer/ShapeSettings/VBoxContainer/HBoxColors/VBoxCircle/CircleTexture/CircleEdge
onready var square_edge: TextureRect = $MenuLayer/ShapeSettings/VBoxContainer/HBoxColors/VBoxSquare/SquareTexture/SquareEdge
onready var triangle_edge: TextureRect = $MenuLayer/ShapeSettings/VBoxContainer/HBoxColors/VBoxTriangle/TriangleTexture/TriangleEdge
onready var color_picker_panel: PanelContainer = $MenuLayer/ColorPickerPanel
onready var color_picker: ColorPicker = $MenuLayer/ColorPickerPanel/ColorPicker
onready var all_slider: Slider = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeSize/GridContainer/AllSlider
onready var player_slider: Slider = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeSize/GridContainer/PlayerSlider
onready var circle_slider: Slider = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeSize/GridContainer/CircleSlider
onready var square_slider: Slider = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeSize/GridContainer/SquareSlider
onready var triangle_slider: Slider = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeSize/GridContainer/TriangleSlider
onready var all_value_label: Label = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeSize/GridContainer/AllValueLabel
onready var player_value_label: Label = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeSize/GridContainer/PlayerValueLabel
onready var circle_value_label: Label = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeSize/GridContainer/CircleValueLabel
onready var square_value_label: Label = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeSize/GridContainer/SquareValueLabel
onready var triangle_value_label: Label = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeSize/GridContainer/TriangleValueLabel
onready var player_button: Button = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeColor/GridContainer/PlayerButton
#onready var bullet_button: Button = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeColor/GridContainer/BulletButton
onready var circle_button: Button = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeColor/GridContainer/CircleButton
onready var square_button: Button = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeColor/GridContainer/SquareButton
onready var triangle_button: Button = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeColor/GridContainer/TriangleButton
onready var reset_edge_colors_button: Button = $MenuLayer/ShapeSettings/VBoxContainer/VBoxEdgeColor/HBoxContainer/ResetEdgeColorsButton

var mouse_over_object: Node
var current_object: Node


func adjust_edge_colors() -> void:
	### instead of 0.4, we could use a variable ('lighten_factor'), and allow
	### user to change the lightening, if override is enabled
	player_edge.self_modulate = player_texture.self_modulate.lightened(0.4)
#	bullet_edge.self_modulate = bullet_texture.self_modulate.lightened(0.4)
	circle_edge.self_modulate = circle_texture.self_modulate.lightened(0.4)
	square_edge.self_modulate = square_texture.self_modulate.lightened(0.4)
	triangle_edge.self_modulate = triangle_texture.self_modulate.lightened(0.4)
	
	
func adjust_edge_size(edge: Node, size: int) -> void:
	var light_res_path = "res://Assets/Light/"

	match edge.get_name():
		"PlayerEdge":
			light_res_path += "Light_Ring_"			
		"CircleEdge":
			light_res_path += "Light_Ring_"
		"SquareEdge":
			light_res_path += "Light_Square_"
		"TriangleEdge":
			light_res_path += "Light_Tri_"

	light_res_path += str(size) + ".png"
	edge.texture = load(light_res_path)


func display_window() -> void:
	shape_settings_panel.show()
	
	
func hide_color_picker() -> void:
	current_object = null
	color_picker_panel.hide()
	
	
func hide_window() -> void:
	shape_settings_panel.hide()
	color_picker_panel.hide()


func load_shape_settings() -> void:	
	edges_enabled_checkbox.pressed = Settings.shape_edges_enabled
	override_edge_checkbox.pressed = Settings.override_shape_edge_colors
	
	player_texture.self_modulate = Settings.player_color
	bullet_texture.self_modulate = Settings.bullet_color
	circle_texture.self_modulate = Settings.circle_color
	square_texture.self_modulate = Settings.square_color
	triangle_texture.self_modulate = Settings.triangle_color
	
	player_slider.value = Settings.player_edge_size + 1
#	bullet_slider.value = Settings.bullet_edge_size + 1
	circle_slider.value = Settings.circle_edge_size + 1
	square_slider.value = Settings.square_edge_size + 1
	triangle_slider.value = Settings.triangle_edge_size + 1
	
	if override_edge_checkbox.pressed:
		player_edge.self_modulate = Settings.player_edge_color
	#	bullet_edge.self_modulate = Settings.bullet_edge_color
		circle_edge.self_modulate = Settings.circle_edge_color
		square_edge.self_modulate = Settings.square_edge_color
		triangle_edge.self_modulate = Settings.triangle_edge_color
	
	toggle_edge_settings(edges_enabled_checkbox.pressed)
	toggle_overrides(override_edge_checkbox.pressed)
	
	
func reset_color(object: Node) -> void:
	var def_color
	match object:
		player_texture:
			def_color = Settings.player_color
		bullet_texture:
			def_color = Settings.bullet_color
		circle_texture:
			def_color = Settings.circle_color
		square_texture:
			def_color = Settings.square_color
		triangle_texture:
			def_color = Settings.triangle_color
	
	object.self_modulate = def_color
	
	if !override_edge_checkbox.pressed:
		adjust_edge_colors()
		
		
func reset_edge_colors() -> void:
	player_edge.self_modulate = player_texture.self_modulate.lightened(0.4)
	circle_edge.self_modulate = circle_texture.self_modulate.lightened(0.4)
	square_edge.self_modulate = square_texture.self_modulate.lightened(0.4)
	triangle_edge.self_modulate = triangle_texture.self_modulate.lightened(0.4)
	pass
		
		
func reset_edge_sizes() -> void:
	all_slider.value = 1
	player_slider.value = Settings.def_edge_size + 1
	circle_slider.value = Settings.def_edge_size + 1
	square_slider.value = Settings.def_edge_size + 1
	triangle_slider.value = Settings.def_edge_size + 1
	
	
func reset_shape_colors() -> void:
	player_texture.self_modulate = Settings.def_player_color
	bullet_texture.self_modulate = Settings.def_bullet_color
	circle_texture.self_modulate = Settings.def_circle_color
	square_texture.self_modulate = Settings.def_square_color
	triangle_texture.self_modulate = Settings.def_triangle_color
	
	if !override_edge_checkbox.pressed:
		adjust_edge_colors()
	
	
func save_shape_settings() -> void:
	Settings.shape_edges_enabled = edges_enabled_checkbox.pressed
	Settings.override_shape_edge_colors = override_edge_checkbox.pressed
	
	Settings.player_color = player_texture.self_modulate
	Settings.bullet_color = bullet_texture.self_modulate
	Settings.circle_color = circle_texture.self_modulate
	Settings.square_color = square_texture.self_modulate
	Settings.triangle_color = triangle_texture.self_modulate
	
	Settings.player_edge_size = player_slider.value - 1
#	Settings.bullet_edge_size = bullet_slider.value - 1
	Settings.circle_edge_size = circle_slider.value - 1
	Settings.square_edge_size = square_slider.value - 1
	Settings.triangle_edge_size = triangle_slider.value - 1
	
	Settings.player_edge_color = player_edge.self_modulate
#	Settings.button_edge_color = button_edge.self_modulate
	Settings.circle_edge_color = circle_edge.self_modulate
	Settings.square_edge_color = square_edge.self_modulate
	Settings.triangle_edge_color = triangle_edge.self_modulate
	
	Settings.save_settings()
	
	
func show_color_picker(object: Node) -> void:
	current_object = object
	color_picker.color = current_object.self_modulate
	color_picker_panel.show()
	
	
func toggle_edge_settings(enabled: bool) -> void:
	override_edge_checkbox.disabled = !enabled
	
	player_edge.visible = enabled
#	bullet_edge.visible = enabled
	circle_edge.visible = enabled
	square_edge.visible = enabled
	triangle_edge.visible = enabled
	
	for child in slider_grid.get_children():
		if child is HSlider:
			child.editable = enabled
			
	if override_edge_checkbox.pressed:
		for child in button_grid.get_children():
			child.disabled = !enabled
		
		
func toggle_overrides(enabled: bool) -> void:
	### make onready vars ?
	player_button.disabled = !enabled
#	bullet_button.disabled = !enabled
	circle_button.disabled = !enabled
	square_button.disabled = !enabled
	triangle_button.disabled = !enabled
	reset_edge_colors_button.disabled = !enabled
	
	if !enabled:
		adjust_edge_colors()


func _on_ShapeSettings_visibility_changed() -> void:
	if shape_settings_panel.visible:
		load_shape_settings()


func _on_EdgesEnabledCheckBox_pressed() -> void:
	toggle_edge_settings(edges_enabled_checkbox.pressed)
	
	if edges_enabled_checkbox.pressed:
		adjust_edge_colors()
	

func _on_mouse_exited() -> void:
	mouse_over_object = null


func _on_OverrideEdgeCheckBox_toggled(button_pressed) -> void:
	toggle_overrides(button_pressed)


func _on_ApplyButton_pressed() -> void:
	save_shape_settings()
	hide_window()
	emit_signal("shape_settings_changed")
	
	
func _on_CancelButton_pressed() -> void:
	hide_window()


func _on_DefaultsButton_pressed() -> void:
	reset_shape_colors()
	reset_edge_colors()
	reset_edge_sizes()


func _on_VBoxPlayer_mouse_entered() -> void:
	mouse_over_object = player_texture


func _on_VBoxBullet_mouse_entered() -> void:
	mouse_over_object = bullet_texture


func _on_VBoxCircle_mouse_entered() -> void:
	mouse_over_object = circle_texture


func _on_VBoxSquare_mouse_entered() -> void:
	mouse_over_object = square_texture


func _on_VBoxTriangle_mouse_entered() -> void:
	mouse_over_object = triangle_texture


func _on_ShapeSettings_gui_input(event) -> void:
	if event.is_action_pressed("mouse_left"):
		if mouse_over_object == null && color_picker_panel.visible:
			hide_color_picker()
		elif mouse_over_object != null && mouse_over_object != color_picker:
			show_color_picker(mouse_over_object)
	elif event.is_action_pressed("mouse_right"):
		if color_picker_panel.visible:
			hide_color_picker()
		else:
			if mouse_over_object != null && mouse_over_object != color_picker:
				reset_color(mouse_over_object)


func _on_ColorPicker_mouse_entered() -> void:
	mouse_over_object = color_picker


func _on_ColorPicker_color_changed(color) -> void:
	if current_object:
		current_object.self_modulate = color
		if !override_edge_checkbox.pressed:
			adjust_edge_colors()


func _on_AllSlider_value_changed(value) -> void:
	all_value_label.set_text(str(value - 1))
	player_slider.value = value
	circle_slider.value = value
	square_slider.value = value
	triangle_slider.value = value


func _on_PlayerSlider_value_changed(value) -> void:
	player_value_label.set_text(str(value - 1))
	adjust_edge_size(player_edge, value - 1)


func _on_CircleSlider_value_changed(value) -> void:
	circle_value_label.set_text(str(value - 1))
	adjust_edge_size(circle_edge, value - 1)


func _on_SquareSlider_value_changed(value) -> void:
	square_value_label.set_text(str(value - 1))
	adjust_edge_size(square_edge, value - 1)


func _on_TriangleSlider_value_changed(value) -> void:
	triangle_value_label.set_text(str(value - 1))
	adjust_edge_size(triangle_edge, value - 1)


func _on_PlayerButton_pressed() -> void:
	show_color_picker(player_edge)


func _on_CircleButton_pressed() -> void:
	show_color_picker(circle_edge)
	
	
func _on_SquareButton_pressed() -> void:
	show_color_picker(square_edge)


func _on_TriangleButton_pressed() -> void:
	show_color_picker(triangle_edge)


func _on_ResetShapeColorsButton_pressed() -> void:
	reset_shape_colors()


func _on_ResetEdgeSizeButton_pressed() -> void:
	reset_edge_sizes()


func _on_ResetEdgeColorsButton_pressed() -> void:
	reset_edge_colors()
