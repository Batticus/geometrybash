extends Node

var custom_label: PackedScene = preload("res://Scenes/UI/CustomLabel.tscn")
var gamestats_ps: Script = preload("res://Scripts/GameStats.gd")
var gamestats: Node
var is_editable: bool = false setget set_editable, get_editable
var selected_highscore: Dictionary

func _ready() -> void:
	gamestats = gamestats_ps.new()
#	$ConfirmationDialog.get_ok().connect("pressed", self, "_on_dialog_ok_pressed")

	populate_highscores()


#func _custom_label_mouse_pressed(label: Label) -> void:
#	if is_editable:
#		selected_highscore = get_selected_highscore(label)	
#		$ConfirmationDialog.dialog_text = "Are you sure you want to remove the selected high score?:"
#		$ConfirmationDialog.popup_centered_minsize()
		
		
#func _on_dialog_ok_pressed() -> void:
#	gamestats.delete_highscore(selected_highscore)
#
#	clear_scores()
#
#	populate_highscores()


func clear_scores(clear_dictionary: bool = false) -> void:
	if clear_dictionary:
		gamestats.clear_highscores()
	
	for label in $ScrollContainer/HighscoreGrid.get_children():
		if "gen_" in label.get_name():
			label.queue_free()


#func get_selected_highscore(label: Label) -> Dictionary:
#	var highscore = {}
#	var label_name = label.get_name()
#	var suffix = label_name.trim_prefix("gen_name")
#
#	highscore["name"] = label.get_text()
#
#	for label in $ScrollContainer/HighscoreGrid.get_children():
#		if label.get_name().ends_with(suffix):
#			match label.get_name().trim_suffix(suffix):
#				"gen_score":
#					highscore["score"] = label.get_text()
#				"gen_kills":
#					highscore["kills"] = label.get_text()
#				"gen_playtime":
#					highscore["playtime"] = label.get_text()
#				"gen_date":
#					highscore["date"] = label.get_text()
#
#	return highscore


func populate_highscores() -> void:
	var name_label
	var score_label
	var kills_label
	var play_time_label
	var date_label
	var idx = 0
	
	gamestats.sort_highscores(gamestats.SortType.SCORE)
	
	for highscore in gamestats.highscores:
		name_label = custom_label.instance()
		score_label = Label.new()
		kills_label = Label.new()
		play_time_label = Label.new()
		date_label = Label.new()
		
		name_label.set_name("gen_name_" + str(idx))
		score_label.set_name("gen_score_" + str(idx))
		kills_label.set_name("gen_kills_" + str(idx))
		play_time_label.set_name("gen_playtime_" + str(idx))
		date_label.set_name("gen_date_" + str(idx))

#		name_label.connect("custom_label_mouse_pressed", self, "_custom_label_mouse_pressed")
		name_label.set_enable_hover_color(false)	#use 'is_editable' instead of 'false' when ready

		name_label.align = Label.ALIGN_CENTER
		score_label.align = Label.ALIGN_CENTER
		kills_label.align = Label.ALIGN_CENTER
		play_time_label.align = Label.ALIGN_CENTER
		date_label.align = Label.ALIGN_CENTER

		name_label.set_text(highscore["name"])
		score_label.set_text(highscore["score"])
		kills_label.set_text(highscore["kills"])
		play_time_label.set_text(highscore["playtime"])
		date_label.set_text(highscore["date"])

		$ScrollContainer/HighscoreGrid.add_child(name_label)
		$ScrollContainer/HighscoreGrid.add_child(score_label)
		$ScrollContainer/HighscoreGrid.add_child(kills_label)
		$ScrollContainer/HighscoreGrid.add_child(play_time_label)
		$ScrollContainer/HighscoreGrid.add_child(date_label)
		
		idx += 1
		
	
func get_editable() -> bool:
	return is_editable
	
	
func set_editable(editable: bool) -> void:
	is_editable = editable
