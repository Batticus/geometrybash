extends ColorRect

onready var settings: Node = $MenuLayer/SettingsWindow
onready var shape_settings: Control = $MenuLayer/SettingsWindow/ShapeSettingsWindow

var keyboard_control: bool = false
var menu_visible: bool = true


### - Built-Ins - ###


func _ready() -> void:	
	settings.connect("close_button_pressed", self, "_on_settings_close_button_pressed")
	settings.connect("settings_applied", self, "_on_settings_applied")
#	settings.connect("settings_window_hidden", self, "_on_settings_window_hidden")
	shape_settings.connect("shape_settings_changed", self, "_on_shape_settings_changed")
	Settings.adjust_global_settings()
	load_applicable_settings()
	
	$MenuLayer/VersionLabel.set_text(Version.get_version())
	
	if Version.is_html5:
		$MenuLayer/FullscreenLabel.show()
		$MenuLayer/VBoxContainer/ExitButton.hide()
	
	### This is done to override default edge color on player
	Settings.save_settings()
	
	MusicManager.play_title()


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_up") or event.is_action_pressed("ui_down"):
		if !keyboard_control:
			keyboard_control = true
			$MenuLayer/VBoxContainer/PlayButton.grab_focus()
			
	if event.is_action_pressed("hide_title"):
		toggle_menu_visibility()
		
#	if event.is_action_pressed("html5_fullscreen_toggle") && Version.is_html5:
#		Settings.fullscreen = !Settings.fullscreen
##		OS.window_fullscreen = !OS.window_fullscreen
##		Settings.fullscreen = OS.window_fullscreen
#		Settings.adjust_display_settings()
				
			
### - Public Methods - ###
			
func adjust_player_properties() -> void:
	var player = $Map_Auto/PlayerShape
	
	player.set_color(Settings.player_color)
	player.set_light_texture_size()
	pass
			
			
func load_applicable_settings() -> void:
	toggle_menu_autoplay(Settings.menu_autoplay)		
	
	
func toggle_menu_autoplay(autoplay: bool) -> void:
	if autoplay:
		$Map_Auto.show()
		### autoplay color
#		self.color = Color("7f000000")
	else:
		$Map_Auto.hide()
		### No autoplay color
#		self.color = Color.black


func toggle_menu_visibility() -> void:
	menu_visible = !menu_visible
	
	$MenuLayer/VBoxContainer.visible = menu_visible
	$MenuLayer/Title.visible = menu_visible
	$MenuLayer/CreditsButton.visible = menu_visible
	$MenuLayer/Panel.visible = menu_visible
		
		
### - Private Methods / Signals


func _on_PlayButton_mouse_entered() -> void:
	$MenuLayer/VBoxContainer/PlayButton.grab_focus()


func _on_HighscoresButton_mouse_entered() -> void:
	$MenuLayer/VBoxContainer/HighscoresButton.grab_focus()


func _on_SettingsButton_mouse_entered() -> void:
	$MenuLayer/VBoxContainer/SettingsButton.grab_focus()


func _on_ExitButton_mouse_entered() -> void:
	$MenuLayer/VBoxContainer/ExitButton.grab_focus()


func _on_PlayButton_pressed() -> void:
	get_tree().change_scene("res://Scenes/Map_0.tscn")
	self.queue_free()
	MusicManager.stop_playback()


func _on_HighscoresButton_pressed() -> void:
	var viewer_ps = load("res://Scenes/Windows/HighScorePanelWButton.tscn")
	var viewer = viewer_ps.instance()
	viewer.get_node("HighScorePanelWButton").set_editable(true)
	add_child(viewer)


func _on_SettingsButton_pressed() -> void:
	settings.show()
	keyboard_control = false
	$MenuLayer/Panel.hide()
	
	### HACK ###
	### A little hack to ensure 'SettingsWindow' has focus
	### and to allow Enter key to apply change
	settings.grab_focus()
	
	
func _on_settings_applied() -> void:
	load_applicable_settings()
	
	
func _on_shape_settings_changed() -> void:
	adjust_player_properties()


func _on_settings_close_button_pressed() -> void:
	settings.hide()
	$MenuLayer/Panel.show()


func _on_ExitButton_pressed() -> void:
	get_tree().quit()

func _on_TextureButtonBosca_pressed():
	OS.shell_open(Settings.bosca_ceoil_url)


func _on_TextureButtonGodot_pressed():
	OS.shell_open(Settings.godot_engine_url)
	
	
func _on_TextureButtonBfxr_pressed():
	OS.shell_open(Settings.bfx_url)
	
	
func _on_TextureButtonAudacity_pressed():
	OS.shell_open(Settings.audacity_url)


func _on_CreditsButton_pressed():
	if $MenuLayer/CreditsWindow.visible:
		$MenuLayer/CreditsWindow.hide()
	else:
		$MenuLayer/CreditsWindow.show()
