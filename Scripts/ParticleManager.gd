### ParticleManager is meant to be attached to any used
### particles. It should be used to control the particle
### in question. It can be thought of as a 'wrapper' around
### a particle node. Toggling 'emitting' directly will from
### the particle node will result it not being freed by
### ParticleManager. Using 'emit()' will begin emitting the
### particle and start the 'free_timer'. Once 'free_timer'
### times out, the particle will be freed from the scene.
### This may not work well for 'non-oneshot' particles,
### unless you only want the particles displayed for a
### certain amount of time.
###
### The export var 'life_time' (Life Time in inspector) is
### used to control how long to wait before freeing the node.
### It should not be confused with the Particle2D node's 'Lifetime'

extends Node

var free_timer: Timer
export (float) var life_time: float = 1.0

func _ready() -> void:
	free_timer = Timer.new()
	free_timer.wait_time = life_time
	free_timer.connect("timeout", self, "_on_life_time_timeout")
	add_child(free_timer)
	

func _on_life_time_timeout() -> void:
	free_timer.stop()
	queue_free()
	

func emit() -> void:
	self.emitting = true
	free_timer.start()
	
	
func determine_rotation(enemy_node: Node) -> void:	
	### This works, but it can look a bit strange at times.
	### If the 'bullet' hits the enemy on the side, the the particles
	### will emit parallel to where the bullet hit (ie. the side of the enemy).
	### Will look into other methods
	var rot = (enemy_node.position - enemy_node.impact_position).angle()
	self.rotation_degrees = rad2deg(rot)
	
	
func set_color(color: Color) -> void:
	self.process_material.color = color
