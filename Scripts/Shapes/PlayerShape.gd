extends "res://Scripts/Shapes/Shape.gd"

### Auto Play Signals ###
signal enemy_detected
signal player_ascended

### Standard Play Signals ###
signal player_hit
signal shoot_standard
#signal shoot_special

export (int) var speed: int

var _iam: bool = false
var _frozen: bool = false setget set_frozen, get_frozen

### Auto Play Variables ###
var is_auto: bool setget set_is_auto, get_is_auto
var enemy_nearby: bool = false
var nearby_enemies: Array
var is_calculating: bool = false

### Standard Play Variables ###
var screen_size: Vector2
var clamp_x: float
var clamp_y: float

func _ready() -> void:
	screen_size = get_viewport_rect().size
	position = Vector2(screen_size.x / 2, screen_size.y / 2)
	clamp_x = $Sprite.texture.get_size().x / 4
	clamp_y = $Sprite.texture.get_size().y / 4
	apply_ring_color()
#	parent = get_parent()
		


func apply_ring_color() -> void:
	if Settings.override_shape_edge_colors:
		$Sprite/Light.self_modulate = Settings.player_edge_color
		$Pointer/Light.self_modulate = Settings.player_edge_color
	else:
		var base_color = $Sprite.self_modulate
		var ring_color = base_color.lightened(0.4)
		$Sprite/Light.self_modulate = ring_color
		$Pointer/Light.self_modulate = ring_color


func control(delta: float) -> void:	
	velocity = Vector2()
	if !is_auto:
#		velocity = Vector2()
		$Pointer.look_at(get_global_mouse_position())
		
		### If '_frozen' is true, then the player can't move.
		### This allows the 'pointer' to keep following the mouse
		if _frozen:
			return
		
		if Input.is_action_pressed("ui_up"):
			velocity += Vector2(0, -1)
		if Input.is_action_pressed("ui_down"):
			velocity += Vector2(0, 1)
		if Input.is_action_pressed("ui_left"):
			velocity += Vector2(-1, 0)
		if Input.is_action_pressed("ui_right"):
			velocity += Vector2(1, 0)
		
#		if velocity.length() > 0:
#			velocity = velocity.normalized() * speed
#
#		position += velocity * delta
#		position.x = clamp(position.x, clamp_x, (screen_size.x - clamp_x))
#		position.y = clamp(position.y, clamp_y, (screen_size.y - clamp_y))
	else:
		if !is_calculating:
			is_calculating = true
			velocity = get_combined_enemy_velocity()
			is_calculating = false
		
	if velocity.length() > 0:
		var cx = clamp_x
		var cy = clamp_y
		if is_auto:
			cx += 50
			cy += 50
			
		velocity = velocity.normalized() * speed
		
		position += velocity * delta
		position.x = clamp(position.x, cx, (screen_size.x - cx))
		position.y = clamp(position.y, cy, (screen_size.y - cy))
		
		
func do_color_correction() -> void:
		toggle_iam_color(_iam)
		
		
func freeze() -> void:
	_frozen = !_frozen
		
		
func hide_light_texture() -> void:
	$Sprite/Light.visible = false
	$Pointer/Light.visible = false
	
	
func get_frozen() -> bool:
	return _frozen
	
	
func set_frozen(frozen: bool) -> void:
	if _frozen == frozen:
		return;
		
	_frozen = frozen
	
	
func set_is_auto(auto: bool) -> void:
	is_auto = auto
	
	
func get_is_auto() -> bool:
	return is_auto
	
	
func set_color(color: Color) -> void:
	$Sprite.self_modulate = color
	$Pointer.self_modulate = color
	apply_ring_color()
	
	
func toggle_iam_color(enable: bool) -> void:
	if enable:
#		set_color(get_color().inverted())
#		set_color(get_color().darkened(0.5))
#		set_color(Color(1.0, 1.0, 1.0, 1.0))
		set_color(Color.white)
	else:
		set_color(Settings.player_color)
	

func get_color() -> Color:
	return $Sprite.self_modulate
	
	
func set_light_texture_size() -> void:
	var player_light_res = "res://Assets/Light/Light_Ring_" + \
			str(Settings.player_edge_size) + ".png"
	var pointer_light_res = "res://Assets/Light/Light_Tri_" + \
			str(Settings.player_edge_size) + ".png"
	
	if Settings.shape_edges_enabled:
		show_light_texture()
		$Sprite/Light.texture = load(player_light_res)
		$Pointer/Light.texture = load(pointer_light_res)
	else:
		hide_light_texture()
	
	
func get_combined_enemy_velocity() -> Vector2:
	var vector = Vector2(0, 0)
	if len(nearby_enemies) > 0:
		for enemy in nearby_enemies:
			vector += enemy.velocity
		
	return vector
	
	
func shoot():
	emit_signal("shoot_standard")
	
	
func show_light_texture() -> void:
	$Sprite/Light.visible = true
	$Pointer/Light.visible = true


func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.is_action_pressed("mouse_left") && !is_auto:
			shoot()
			
	if event.is_action_pressed("iam"):
		_iam = !_iam
		toggle_iam_color(_iam)
		emit_signal("player_ascended")
#		if !_iam:
##			set_color(Settings.player_color.inverted())
#			set_color(get_color().inverted())
#		else:
##			self.modulate = Settings.player_color
#			set_color(Settings.player_color)
#
#		_iam = !_iam


func _on_PlayerShape_body_entered(body: Node) -> void:
		if "Circle" or "Square" or "Triangle" in body.get_name():
			if !_iam:
				emit_signal("player_hit")
			else:
				body.queue_free()
		
###	If the player is to have the ability to shoot only
###	at specific intervals (i.e. 0.25ms), and can shoot
### continuously while the left mouse button is held,
### use the code function below and comment/remove the
### "func _input(event):" code.
### This requires a timer's to be connected to this
###	function

#func _on_Timer_timeout():
#	if Input.is_action_pressed("mouse_left"):
#		emit_signal("shoot_standard")


func _on_AutoDetectArea_body_entered(body: Node) -> void:
	if body.is_in_group("Enemy"):
		nearby_enemies.append(body)
#		emit_signal("enemy_detected", body)


func _on_AutoDetectArea_body_exited(body):
	nearby_enemies.erase(body)
#	is_engaged = false
