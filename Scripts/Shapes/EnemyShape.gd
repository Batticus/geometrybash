extends KinematicBody2D

signal enemy_destroyed
#signal color_changed

export (int) var speed: int

var player_node: Area2D
var player_alive: bool
var velocity: Vector2 = Vector2()
var impact_position: Vector2

func _ready() -> void:
	player_node = get_parent().get_node("PlayerShape")
	player_alive = get_parent().has_node("PlayerShape")
	apply_ring_color()


func _process(delta: float) -> void:
	velocity = Vector2()
	if get_parent().has_node("PlayerShape"):
#	if player_node:
#	if player_alive:
		follow_player(delta)
	else:
		amble_about(delta)
		pass # Player dead. Do something!


func amble_about(delta: float) -> void:
	pass


func follow_player(delta: float) -> void:
	var direction = player_node.position - position
	look_at(player_node.position)
	velocity = direction.normalized() * speed
	move_and_collide(velocity * delta)
	#move_and_slide(velocity * delta) # ???
	
	
func hide_light_texture() -> void:
	$Sprite/Light.visible = false
	

func apply_ring_color() -> void:
	if Settings.override_shape_edge_colors:
		$Sprite/Light.self_modulate = Settings.get_light_color(self)
	else:
		var base_color = $Sprite.self_modulate
		var ring_color = base_color.lightened(0.4)
		$Sprite/Light.self_modulate = ring_color
	pass
	

func set_color(color: Color) -> void:
	$Sprite.self_modulate = color
	apply_ring_color()
#	emit_signal("color_changed")

func get_color() -> Color:
	### ?
#	if randi() % 2 == 1:
#		return $Sprite/Light_Ring_3.self_modulate
#	else:
#		return $Sprite.self_modulate
	return $Sprite.self_modulate
	
	
func set_light_texture(light_texture: Texture) -> void:
	$Sprite/Light.texture = light_texture
	
	
func show_light_texture() -> void:
	$Sprite/Light.visible = true
	
	
#func set_light_color(color: Color) -> void:
#	$Sprite/Light.self_modulate = color


func destroy(impact_pos: Vector2) -> void:
	impact_position = impact_pos
	emit_signal("enemy_destroyed", self)
	queue_free()
