extends Area2D

#signal bullet_freed

export (int) var speed: int

var velocity: Vector2 = Vector2()

func fire(bullet_start: Vector2, target: Vector2) -> void:
	position = bullet_start
	var dir = target - global_position
	velocity = dir.normalized() * speed
	$Lifetime.start()
	
	
func _process(delta: float) -> void:
	#set_process(false) # What is this for? See Top down tank battle by KidsCanCode
	#velocity = velocity.clamped(speed) # ? Also see KidsCanCode
	if is_out_of_bounds():
		set_process(false)
		queue_free()
#		emit_signal("bullet_freed")
	position += velocity * delta


func _on_Bullet_body_entered(body: PhysicsBody2D) -> void:
	if body.has_method("destroy"):
		body.destroy(self.position)
		$Sprite.hide()
		queue_free()


func is_out_of_bounds() -> bool:
	var screen_size = get_viewport_rect().size
	if position.x < 0 or position.x > screen_size.x or position.y < 0 or position.y > screen_size.y:
		return true
	return false
	
	
func set_color(color: Color) -> void:
	$Sprite.self_modulate = color
	
	
func get_color() -> Color:
	return $Sprite.self_modulate


func _on_Lifetime_timeout() -> void:
	$Lifetime.stop()
	queue_free()
#	emit_signal("bullet_freed")
