extends Node2D

enum TRACKS {
	GeoBash = 1,
	Destruction = 2,
	Angular_Anarchy = 3
}

export (float) var db_limit: float = -1.0

onready var music_bus_index: int = AudioServer.get_bus_index("Music")

var current_track: int
var last_track: int
var play_sequentially: bool
var played_tracks: Array
var track_stream: AudioStream
var playing: bool
var paused: bool
var pause_pos: float
var title_track_path: String = "res://Assets/Music/Title.ogg"

var track_paths: Dictionary = {
	1: "res://Assets/Music/GeoBash.ogg",
	2: "res://Assets/Music/Destruction.ogg",
	3: "res://Assets/Music/Angular Anarchy.ogg"
}

#export (Dictionary) var track_levels: Dictionary = {
#	1: 0.0,
#	2: -2.5,	#?
#	3: -3		#?
#}


#func _ready() -> void:
#	$AudioStreamPlayer.set_bus("Music")
	
	
func next_track() -> void:
	if current_track == 3:
		play_track(1)
	else:
		play_track(current_track + 1)


func play_next_random() -> void:
	var track_number = randi() % track_paths.size() + 1
	
	if played_tracks.size() == track_paths.size():
		last_track = current_track
		played_tracks.clear()
	
#	if track_paths[track_number] in played_tracks
	if track_paths[track_number] in played_tracks or track_number == last_track:
		play_next_random()
	else:
		last_track = 0
		played_tracks.append(track_paths[track_number])
		play_track(track_number)
		current_track = track_number
	
	
func play_next_sequential() -> void:
	### If 'current_track' is the last track, start from the beginning
	if current_track == track_paths.size():
		play_track(1)
	### play next track in TRACKS
	else:
		play_track(current_track + 1)
		
		
func play_title() -> void:
	track_stream = load(title_track_path)
	$AudioStreamPlayer.stream = track_stream
	playing = true
	$AudioStreamPlayer.play()
	$AudioStreamPlayer.volume_db = db_limit


func play_track(track_num: int) -> void:
	track_stream = load(track_paths[track_num])
	$AudioStreamPlayer.stream = track_stream
	playing = true
	$AudioStreamPlayer.play()
	current_track = track_num
#	$AudioStreamPlayer.volume_db = track_levels[track_num]
	$AudioStreamPlayer.volume_db = db_limit
	
	
func previous_track() -> void:
	if current_track == 1:
		play_track(3)
	else:
		play_track(current_track - 1)


func start_playback(sequential: bool = true) -> void:
	play_sequentially = sequential
	if play_sequentially:
		play_track(1)
	else:
		play_next_random()
		
		
func stop_playback() -> void:
	playing = false
	$AudioStreamPlayer.stop()
	
	
func toggle_pause() -> void:
	if !paused:
		pause_pos = $AudioStreamPlayer.get_playback_position()
		playing = false
		paused = true
		$AudioStreamPlayer.stop()		
	else:		
		playing = true
		paused = false
		$AudioStreamPlayer.play()
		$AudioStreamPlayer.seek(pause_pos)
		


func _on_Timer_timeout():
	$Timer.stop()
	if play_sequentially:
		play_next_sequential()
	else:
		play_next_random()


func _on_AudioStreamPlayer_finished():
	if playing:
		$Timer.start()
#		if play_sequentially:
#			play_next_sequential()
#		else:
#			play_next_random()
