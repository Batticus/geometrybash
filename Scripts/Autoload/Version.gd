extends Node
### I 'KNOW' there's a better way, I just know it!?!?

const _version_num: String = "1.0.6"
var is_html5: bool


func _ready() -> void:
	is_html5 = OS.has_feature("HTML5")


func get_version() -> String:
	var is_x11 = OS.has_feature("X11")
	var is_64 = OS.has_feature("64")
	var version = _version_num + "-"
	
	if is_html5:
		version += "html5"
	elif is_x11:
		version += "linux/x11-"
	else:
		version += "win-"
		
	if !is_html5:
		if is_64:
			version += "64bit"
		else:
			version += "32bit"
			
	return (version)
