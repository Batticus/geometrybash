extends Node

const settings_path: String = "user://settings.dat"
const reddit_thread_url: String = "https://www.reddit.com/r/godot/comments/ep2sz6/particle2d_emission_problem/"
const bosca_ceoil_url: String = "https://boscaceoil.net"
const godot_engine_url: String = "https://godotengine.org"
const bfx_url: String = "https://www.bfxr.net/"
const audacity_url: String = "https://www.audacityteam.org/"
const dafont_font_page = "https://www.dafont.com/a-galega.font"
const dafont_creator_profile = "https://www.dafont.com/profile.php?user=64690"

const def_player_color: String = "0040ff"
const def_bullet_color: String = "ffffff"
const def_circle_color: String = "ac0004"
const def_square_color: String = "ac0004"
const def_triangle_color: String = "ac0004"
const def_glow_enabled: bool = true
const def_glow_intensity: float = 8.0
const def_glow_strength: float = 0.9
const def_glow_bloom: float = 0.05
const def_glow_blend_mode: int = VisualServer.GLOW_BLEND_MODE_ADDITIVE
const def_glow_bicubic: bool = true
const def_shape_edges_enabled: bool = true
const def_override_shape_edge_colors: bool = false
const def_edge_color: Color = Color(0, 0, 0, 0)
const def_edge_size: int = 3
const resolutions: Array = [
	"640×480",
	"800×600",
	"960×720",
	"1024×576",
	"1024x600",
	"1024×768",
	"1152×648",
	"1280×720",
	"1280×800",
	"1280×960",
	"1360x768",
	"1366×768",
	"1400×1050",
	"1440×900",
	"1440×1080",
	"1600×900",
	"1600×1200",
	"1680×1050",
	"1856×1392",
	"1920×1080",
	"1920×1200",
	"1920×1440",
	"2048×1536",
	"2560×1440",
	"2560×1600",
	"3840×2160",
	"7680x4320"
]

var master_volume : float
var music_volume : float
var effects_volume : float
var player_color : Color
var circle_color : Color
var square_color : Color
var triangle_color : Color
var bullet_color : Color
var fullscreen : bool
var menu_autoplay: bool
var glow_enabled: bool
var glow_intensity: float
var glow_strength: float
var glow_bloom: float
var shape_edges_enabled: bool
var override_shape_edge_colors: bool
var player_edge_color: Color
var circle_edge_color: Color
var square_edge_color: Color
var triangle_edge_color: Color
var player_edge_size: int
var circle_edge_size: int
var square_edge_size: int
var triangle_edge_size: int

var use_basic_font: bool

var settings_file : File

onready var master_audio_bus: int = AudioServer.get_bus_index("Master")
onready var music_audio_bus: int = AudioServer.get_bus_index("Music")
onready var effects_audio_bus: int = AudioServer.get_bus_index("Effects")

func _ready() -> void:
	settings_file = File.new()
	get_viewport().connect("size_changed", self, "_on_size_changed")
	
	if settings_file.file_exists(settings_path):
		load_settings()
	else:
		set_defaults()
	
#	adjust_display_settings()
	adjust_audio_settings()
	

func adjust_display_settings() -> void:
	OS.window_fullscreen = fullscreen
	
	if fullscreen:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
		
	if Version.is_html5:
		get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_2D, SceneTree.STRETCH_ASPECT_KEEP, Vector2(1024, 600), 1)
	
	"""
	*** To have the entire screen a single, changeable color,
	*** Map_0/ColorRect should be hidden, or removed, and the
	*** use VisualServer.set_default_clear_color(Color(r, g, b, a)).
	
	>>> VisualServer.set_default_clear_color(Color(0.05,0.05,0.05,1.0))
	"""

func adjust_audio_settings() -> void:
	AudioServer.set_bus_volume_db(master_audio_bus, linear2db(master_volume))
	AudioServer.set_bus_volume_db(music_audio_bus, linear2db(music_volume))
	AudioServer.set_bus_volume_db(effects_audio_bus, linear2db(effects_volume))


func adjust_global_settings() -> void:
	adjust_display_settings()
	adjust_audio_settings()
	
	
func get_light_color(shape: Node) -> Color:
	var color
	
	if "Circle" in shape.get_name():
		color = circle_edge_color
	elif "Square" in shape.get_name():
		color = square_edge_color
	elif "Triangle" in shape.get_name():
		color = triangle_edge_color
	
	return color
	
''' Used in-game for obtaining light textures '''
func get_light_res_path(shape: Node) -> String:
	var light_res_path = "res://Assets/Light/"
	var shape_name = shape.get_name()
	var edge_size
	
	if "Player" in shape_name:
		light_res_path += "Light_Ring_"
		edge_size = player_edge_size
	elif "Circle" in shape_name:
		light_res_path += "Light_Ring_"
		edge_size = circle_edge_size
	elif "Square" in shape_name:
		light_res_path += "Light_Square_"
		edge_size = square_edge_size
	elif "Triangle" in shape_name:
		light_res_path += "Light_Tri_"
		edge_size = triangle_edge_size
		
	light_res_path += str(edge_size) + ".png"
	
	return light_res_path
	
	
#func get_version_info() -> void:
#	var export_conf = ConfigFile.new()
#	var error = export_conf.load("res://export_presets.cfg")
##	var file_version = export_conf.get_value("preset.0.options", "application/file_version", "file version")
#	var version = export_conf.get_value("preset.0.options", "application/product_version")
##	var is_x64 = export_conf.get_value("preset.0.options", "binary_format/64_bits", false)
#	var is_x64 = OS.has_feature("64")
#
#	is_html5 = OS.has_feature("HTML5")
#	is_x11 = OS.has_feature("X11")
#
#	if version:
#		version_string = version + "-"
##	if is_html5:
##		version_string += "html5"
##	elif is_x11:
##		version_string += "linux/x11"
##	else:
##		version_string += "win-"
##		if is_x64:
##			version_string += "64bit"
##		else:
##			version_string += "32bit"
#
#	print("Getting Version Info")
#	if is_html5:
#		version_string += "html5"
#	elif is_x11:
#		version_string += "linux/x11-"
#	else:
#		version_string += "win-"
#
#	if !is_html5:
#		if is_x64:
#			version_string += "64bit"
#		else:
#			version_string += "32bit"


func open_settings_write() -> void:
	var error = settings_file.open(settings_path, File.WRITE)
	
	if error != OK:
		print("ERROR: 'Settings.gd -> open_settings_write()' : "
			+ error.keys()[error])
	
	
func open_settings_read() -> void:
	var error = settings_file.open(settings_path, File.READ)
	
	if error != OK:
		print("ERROR: 'Settings.gd -> open_settings_read()' : "
			+ error.keys()[error])


func settings_file_exists() -> bool:
	return settings_file.file_exists(settings_path)
	
	
func toggle_mute_audio(mute: bool) -> void:
	AudioServer.set_bus_mute(master_audio_bus, mute)
	

func toggle_mute_effects(mute: bool) -> void:
	AudioServer.set_bus_mute(effects_audio_bus, mute)

	
func toggle_mute_music(mute: bool) -> void:
	AudioServer.set_bus_mute(music_audio_bus, mute)


func set_defaults() -> void:
	master_volume = 1
	music_volume = 1
	effects_volume = 1
	player_color = Color(def_player_color)
	circle_color = Color(def_circle_color)
	square_color = Color(def_square_color)
	triangle_color = Color(def_triangle_color)
	bullet_color = Color(def_bullet_color)
	fullscreen = false
	menu_autoplay = true
	glow_enabled = def_glow_enabled
	glow_intensity = def_glow_intensity
	glow_strength = def_glow_strength
	glow_bloom = def_glow_bloom
	shape_edges_enabled = def_shape_edges_enabled
	override_shape_edge_colors = def_override_shape_edge_colors
	player_edge_color = def_edge_color
	circle_edge_color = def_edge_color
	square_edge_color = def_edge_color
	triangle_edge_color = def_edge_color
	player_edge_size = def_edge_size
	circle_edge_size = def_edge_size
	square_edge_size = def_edge_size
	triangle_edge_size = def_edge_size
	use_basic_font = false
	
	if player_edge_color == def_edge_color:
		if !override_shape_edge_colors:
			player_edge_color = player_color.lightened(0.4)
	
	save_settings()


func load_settings() -> void:
	open_settings_read()
	
	master_volume = settings_file.get_var()
	music_volume = settings_file.get_var()
	effects_volume = settings_file.get_var()
	player_color = settings_file.get_var()
	circle_color = settings_file.get_var()
	square_color = settings_file.get_var()
	triangle_color = settings_file.get_var()
	bullet_color = settings_file.get_var()
	fullscreen = settings_file.get_var()
	menu_autoplay = settings_file.get_var()
	glow_enabled = settings_file.get_var()
	glow_intensity = settings_file.get_var()
	glow_strength = settings_file.get_var()
	glow_bloom = settings_file.get_var()
	shape_edges_enabled = settings_file.get_var()
	override_shape_edge_colors = settings_file.get_var()
	player_edge_color = settings_file.get_var()
	circle_edge_color = settings_file.get_var()
	square_edge_color = settings_file.get_var()
	triangle_edge_color = settings_file.get_var()
	player_edge_size = settings_file.get_var()
	circle_edge_size = settings_file.get_var()
	square_edge_size = settings_file.get_var()
	triangle_edge_size = settings_file.get_var()
	use_basic_font = settings_file.get_var()
	
	settings_file.close()


func save_settings() -> void:
	open_settings_write()

	settings_file.store_var(master_volume)
	settings_file.store_var(music_volume)
	settings_file.store_var(effects_volume)
	settings_file.store_var(player_color)
	settings_file.store_var(circle_color)
	settings_file.store_var(square_color)
	settings_file.store_var(triangle_color)
	settings_file.store_var(bullet_color)
	settings_file.store_var(fullscreen)
	settings_file.store_var(menu_autoplay)
	settings_file.store_var(glow_enabled)
	settings_file.store_var(glow_intensity)
	settings_file.store_var(glow_strength)
	settings_file.store_var(glow_bloom)
	settings_file.store_var(shape_edges_enabled)
	settings_file.store_var(override_shape_edge_colors)
	settings_file.store_var(player_edge_color)
	settings_file.store_var(circle_edge_color)
	settings_file.store_var(square_edge_color)
	settings_file.store_var(triangle_edge_color)
	settings_file.store_var(player_edge_size)
	settings_file.store_var(circle_edge_size)
	settings_file.store_var(square_edge_size)
	settings_file.store_var(triangle_edge_size)
	settings_file.store_var(use_basic_font)
	
	settings_file.close()
