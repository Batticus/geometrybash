extends "res://Scripts/Map_0.gd"

export (float) var autoplay_spawn_time: float = 0.4

var closest_enemy: Node
var player_dead: bool = false
var wait: bool = false

onready var restart_timer: Timer = Timer.new()

func _ready() -> void:
	_player.connect("enemy_detected", self, "_on_enemy_detected")
	### Adding to root may be why restart timer isn't working
#	get_node("/root").add_child(restart_timer)
#	restart_timer.wait_time = 2.0
#	restart_timer.connect("timeout", self, "_on_RestartTimer_timeout")
	
	if is_static:
#		Settings.toggle_mute_audio(true)
#		Settings.toggle_mute_effects(true)
		$Explosion.volume_db = -INF
#		remove_child($WorldEnvironment)
		$HUD/Kills.hide()
		$HUD/Points.hide()
		$HUD/TimePlayed.hide()


func start_game() -> void:
	toggle_autoplay()
	spawn_time = autoplay_spawn_time
	.start_game()
	
	
func _input(event: InputEvent) -> void:
	##!BROKEN!
	if event.is_action_pressed("toggle_autoplay"):
		toggle_autoplay()
	
	
####+	Auto Tests	+####

### Not sure about this...?
func _process(delta: float) -> void:
	if len(get_tree().get_nodes_in_group("Bullet")) < 1:
		wait = false

	if !wait:
		if _autoplay && !player_dead:
			var enemies = get_tree().get_nodes_in_group("Enemy")
			var distance
			var pdistance
			if len(enemies) > 0 && !player_dead:
				for cnt in range(0, len(enemies)):
					distance = $PlayerShape.position.distance_to(enemies[cnt].position)
					if !pdistance:
						pdistance = distance
						closest_enemy = enemies[cnt]
					elif distance < pdistance:
						closest_enemy = enemies[cnt]
	#				$PlayerShape.look_at(closest_enemy.position)
				player_shoot()
			
			
#func _on_bullet_freed() -> void:
#	wait = false
	
	
func _on_player_hit() -> void:
	player_dead = true
	if is_static:
		restart_game()
		return
	Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
	._on_player_hit()
			
			
func _on_PlayerShape_shoot_standard() -> void:
	var bullet = bullet_ps.instance()
#	bullet.connect("bullet_freed", self, "_on_bullet_freed")
	bullet.modulate = Settings.bullet_color
	add_child(bullet)
	bullet.fire($PlayerShape/Pointer/BulletStart.global_position, closest_enemy.position)
#	$PlayerShape/ShootSound.play()
#	._on_PlayerShape_shoot_standard()


func _on_game_paused() -> void:
	paused = !paused
	if !paused:
		if !_player._iam:
			_player.modulate = Settings.player_color
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
		
		
func _on_RestartTimer_timeout() -> void:
	restart_timer.stop()
	get_tree().reload_current_scene()
		
		
func end_game() -> void:
#	if _autoplay:
#		if is_static:
##			restart_timer.start()
##			get_tree().reload_current_scene()
#			$PlayerShape.hide()
#			restart()
#		else:
	show_game_over_menu()


func player_shoot():
	wait = true
#	get_viewport().warp_mouse(closest_enemy.position)
	_player.look_at(closest_enemy.position)
	_player.call_deferred("shoot")
#	$PlayerShape.shoot()


func restart_game() -> void:
	$PlayerShape.position = player_start
	$PlayerShape.show()
	player_dead = false
	for enemy in get_tree().get_nodes_in_group("Enemy"):
		enemy.queue_free()
	
	
func toggle_autoplay() -> void:
	_autoplay = !_autoplay
	_player.set_is_auto(_autoplay)
	if _autoplay && !is_static:
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
		
		
func _on_shape_settings_changed() -> void:
	_player.set_light_texture_size()
	pass
